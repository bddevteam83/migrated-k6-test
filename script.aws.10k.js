import "./libs/shim/core.js";
import {
    group,
    sleep
} from "k6";


export let options = {
    maxRedirects: 4,
    duration: "4m15s",
    vus: 2000
};

const env = {
    hostname: "a59f94c31bd6840689c613187a4ed04a-2116872383.eu-west-3.elb.amazonaws.com:8442",
    serviceSmsName: "SMS hDYIx",
    serviceDataName: "Internet Psvgu",
    serviceVoiceName: "VoiceCall ZUfyH"
};

const Request = Symbol.for("request");
postman[Symbol.for("initial")]({
    options,
    environment: env
});

const FileData = JSON.parse(open("./data.aws.10k.json"));

export default function() {
    //let maxIndex = 20000;
    let index = __VU; //+ __ITER;
    // if (index > maxIndex) {
    //   index -= maxIndex
    // }
    let subscriberId = FileData.subscribers[index];

    group("SMS", function() {
        postman[Request]({
            name: "Rating SMS event",
            id: "247edf4c-5b10-4f36-bd3c-d4527807803f",
            method: "POST",
            address: "http://{{hostname}}/events?offline=true",
            data: JSON.stringify({
                serviceName: env.serviceSmsName,
                ratingParameters: ["+38063","+38044"],
                initiatorIdentifier: subscriberId,
                realm: "a",
                originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
                externalSessionId: "{{$guid}}"
            }),
            headers: {
                "Content-Type": "application/json"
            },
            post(response) {
                tests["Successful POST request"] = responseCode.code === 200;
            }
        });
    });

    group("INTERNET DATA", function() {
        let sessionIdInternetData = ""

        postman[Request]({
            name: "Rating event internet data first",
            id: "aa465d80-8b1f-4058-9396-1e568846b715",
            method: "POST",
            address: "http://{{hostname}}/events?offline=false",
            data: JSON.stringify({
                serviceName: env.serviceDataName,
                ratingParameters: ["+38066","+38044"],
                initiatorIdentifier: subscriberId,
                realm: "a",
                originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
                externalSessionId: "{{$guid}}",
                quota: 1,
                quotaUnit: "MB"
            }),
            headers: {
                "Content-Type": "application/json"
            },
            post(response) {
                tests["Successful POST request"] = responseCode.code === 200;

                var jsonData = JSON.parse(responseBody);
                sessionIdInternetData = jsonData.sessionId;
            }
        });

        for (let i = 0; i < 100; i++) {
            postman[Request]({
                name: "Rating Internet Data continue",
                id: "a3ce53db-63ca-4d54-8fdb-33c70b54ff13",
                method: "POST",
                address: "http://{{hostname}}/events?offline=false",
                data: JSON.stringify({
                    serviceName: env.serviceDataName,
                    ratingParameters: ["+38066", "+38044"],
                    initiatorIdentifier: subscriberId,
                    realm: "a",
                    originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
                    externalSessionId: "{{$guid}}",
                    sessionId: sessionIdInternetData,
                    quota: 1,
                    quotaUnit: "MB"
                }),
                headers: {
                    "Content-Type": "application/json"
                },
                post(response) {
                    tests["Successful POST request"] = responseCode.code === 200;
                }
            });
        }
        postman[Request]({
            name: "Rating Internet Data stop",
            id: "a3ce53db-63ca-4d54-8fdb-33c70b54ff15",
            method: "POST",
            address: "http://{{hostname}}/events?offline=false",
            data: JSON.stringify({
                serviceName: env.serviceDataName,
                ratingParameters: ["+38066", "+38044"],
                initiatorIdentifier: subscriberId,
                realm: "a",
                originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
                externalSessionId: "{{$guid}}",
                sessionId: sessionIdInternetData,
                eventType: "STOP",
                quota: 1,
                quotaUnit: "MB"
            }),
            headers: {
                "Content-Type": "application/json"
            },
            post(response) {
                tests["Successful POST request"] = responseCode.code === 200;
            }
        });
    });

     group("VOICE", function() {
       let sessionIdVoiceCall = ""

       postman[Request]({
         name: "Rating event voice call first",
         id: "aa465d80-8b1f-4058-9396-1e568846b715",
         method: "POST",
         address: "http://{{hostname}}/events?offline=false",
         data: JSON.stringify({
           serviceName: env.serviceVoiceName,
           ratingParameters: ["+38063","+38044"],
           initiatorIdentifier: subscriberId,
           realm: "a",
           originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
           externalSessionId: "{{$guid}}",
           quota: 1,
           quotaUnit: "SEC",
           eventType: null
         }),
         headers: {
           "Content-Type": "application/json"
         },
         post(response) {
           tests["Successful POST request"] = responseCode.code === 200;

           var jsonData = JSON.parse(responseBody);
           sessionIdVoiceCall = jsonData.sessionId;
         }
       });

       postman[Request]({
         name: "Rating event voice call start",
         id: "a3ce53db-63ca-4d54-8fdb-33c70b54ff13",
         method: "POST",
         address: "http://{{hostname}}/events?offline=false",
         data: JSON.stringify({
             serviceName: env.serviceVoiceName,
             ratingParameters: ["+38063","+38044"],
             initiatorIdentifier: subscriberId,
             realm: "a",
             originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
             externalSessionId: "{{$guid}}",
             quota: 1,
             quotaUnit: "SEC",
             eventType: "START",
             sessionId: sessionIdVoiceCall
         }),
         headers: {
           "Content-Type": "application/json"
         },
         post(response) {
           tests["Successful POST request"] = responseCode.code === 200;
         }
       });
       sleep(60); // one minute voice call

       postman[Request]({
         name: "Rating event voice call stop",
         id: "bf0ea23b-6d2d-4ad7-9e24-048bd8f708f4",
         method: "POST",
         address: "http://{{hostname}}/events?offline=false",
         data: JSON.stringify({
             serviceName: env.serviceVoiceName,
             ratingParameters: ["+38063","+38044"],
             initiatorIdentifier: subscriberId,
             realm: "a",
             originateTime: "2020-10-21T16:30:00.289+03:00[UTC]",
             externalSessionId: "{{$guid}}",
             quota: 1,
             quotaUnit: "SEC",
             eventType: "STOP",
             sessionId: sessionIdVoiceCall
         }),
         headers: {
           "Content-Type": "application/json"
         },
         post(response) {
           tests["Successful POST request"] = responseCode.code === 200;
         }
       });
     });
}

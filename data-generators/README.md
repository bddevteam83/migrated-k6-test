# README #

This README document whatever steps are necessary to get data ready for loadtesting.

### How do I get data prepared for load testing? ###

1. Create catalog data (services, tariffs and so on). You can apply data dump (dump_21-10-2020_11_39_29.sql) prepared by QA.
We'd recommend use psql tool to apply/build data dumps for Postgres. For ex, here is the command to apply dump to a remote POstgres DB:
`psql -h a3687917c44fb48baab25e6233de2607-82680681.eu-west-3.elb.amazonaws.com -p 5432 -U mcadmin rater -a -f "/Users/user/Downloads/dump_21-10-2020_11_39_29.sql"`

2. Fetch catalog data for the data-generator script.
Instead of fetching catalog data manually use the `flo_get_data_for_generator.txt` script

3. Update `flo_generate_data.txt` with ID fetched on step #2. ID which should be verified and updated - productOffering, serviceId, tariffPlanId and so on

4. Execute `flo_generate_data.txt` 
NOTE: update LOOP iterations count to reach desired quantity of subscribers

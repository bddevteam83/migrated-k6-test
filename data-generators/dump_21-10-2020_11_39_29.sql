--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: get_compatible_sets_count(uuid); Type: FUNCTION; Schema: public; Owner: mcadmin
--

CREATE FUNCTION public.get_compatible_sets_count(offer_id uuid) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE compatible_sets_count integer;
BEGIN
   SELECT Count(cs.*)
   INTO   compatible_sets_count
   FROM   catalog_product_offering cpo
       INNER JOIN product_offering_to_compatibility_set potcs
               ON cpo.id = potcs.product_offering_id
       INNER JOIN compatibility_set cs
               ON potcs.compatibility_set_id = cs.id
   WHERE  cpo.id = offer_id
       AND cs.is_deleted IS FALSE
       AND cpo.is_deleted IS FALSE;
 RETURN compatible_sets_count;
END
;
$$;


ALTER FUNCTION public.get_compatible_sets_count(offer_id uuid) OWNER TO mcadmin;

--
-- Name: get_inclusive_compatible_sets_count(uuid); Type: FUNCTION; Schema: public; Owner: mcadmin
--

CREATE FUNCTION public.get_inclusive_compatible_sets_count(offer_id uuid) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE inclusive_compatible_sets_count integer;
BEGIN
   SELECT Count(cs.*)
   INTO   inclusive_compatible_sets_count
   FROM   catalog_product_offering cpo
          INNER JOIN product_offering_to_compatibility_set potcs
                  ON cpo.id = potcs.product_offering_id
          INNER JOIN compatibility_set cs
                  ON potcs.compatibility_set_id = cs.id
   WHERE  cpo.id = offer_id
          AND cs.type = 'INCLUSIVE'
          AND cs.is_deleted IS FALSE
          AND cpo.is_deleted IS FALSE;
RETURN inclusive_compatible_sets_count;
END
;
$$;


ALTER FUNCTION public.get_inclusive_compatible_sets_count(offer_id uuid) OWNER TO mcadmin;

--
-- Name: get_offers_count(uuid); Type: FUNCTION; Schema: public; Owner: mcadmin
--

CREATE FUNCTION public.get_offers_count(offer_id uuid) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE offers_count integer;
BEGIN
  SELECT Count(cpo.*)
  INTO   offers_count
  FROM   catalog_product_offering cpo
  WHERE  cpo.id = offer_id;

  RETURN offers_count;
END ;$$;


ALTER FUNCTION public.get_offers_count(offer_id uuid) OWNER TO mcadmin;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: catalog_balance; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_balance (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    description character varying(2048),
    type character varying(32) NOT NULL,
    currency character varying(32) NOT NULL,
    is_virtual boolean DEFAULT false,
    minimum_balance bigint,
    maximum_balance bigint,
    is_deleted boolean DEFAULT false,
    name character varying(256),
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_balance OWNER TO mcadmin;

--
-- Name: catalog_bundle_to_nested_product_offer; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_bundle_to_nested_product_offer (
    bundle_product_offer uuid NOT NULL,
    nested_product_offer uuid NOT NULL
);


ALTER TABLE public.catalog_bundle_to_nested_product_offer OWNER TO mcadmin;

--
-- Name: catalog_calendar; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_calendar (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    version bigint,
    name character varying(256) NOT NULL,
    description character varying(2048),
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_calendar OWNER TO mcadmin;

--
-- Name: catalog_calendar_properties; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_calendar_properties (
    id uuid NOT NULL,
    calendar uuid,
    version bigint,
    name character varying(256) NOT NULL,
    month_from integer NOT NULL,
    month_to integer,
    dom_from integer,
    dom_to integer,
    time_from time without time zone,
    time_to time without time zone,
    is_deleted boolean DEFAULT false,
    year_from integer DEFAULT 0,
    year_to integer,
    dow_from integer DEFAULT 0,
    dow_to integer,
    description character varying(2048),
    color character varying(255),
    special boolean DEFAULT false NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_calendar_properties OWNER TO mcadmin;

--
-- Name: catalog_custom_list; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_custom_list (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(2048),
    list jsonb,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_custom_list OWNER TO mcadmin;

--
-- Name: catalog_layout; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_layout (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    version bigint,
    rating_parameter_set uuid,
    name character varying(256) NOT NULL,
    description character varying(2048),
    data jsonb,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_layout OWNER TO mcadmin;

--
-- Name: catalog_layout_to_layout_rating_parameters; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_layout_to_layout_rating_parameters (
    catalog_layout uuid NOT NULL,
    layout_rating_parameters uuid NOT NULL
);


ALTER TABLE public.catalog_layout_to_layout_rating_parameters OWNER TO mcadmin;

--
-- Name: catalog_offer_price; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_offer_price (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    currency character(3) NOT NULL,
    value bigint NOT NULL
);


ALTER TABLE public.catalog_offer_price OWNER TO mcadmin;

--
-- Name: catalog_offer_term; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_offer_term (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    type character varying(32) NOT NULL,
    recurring_type uuid NOT NULL,
    tariff_plan uuid,
    offer_priority integer,
    allowance integer,
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone
);


ALTER TABLE public.catalog_offer_term OWNER TO mcadmin;

--
-- Name: catalog_price_item; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_price_item (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    version bigint,
    currency character(3),
    name character varying(256) NOT NULL,
    description character varying(2048),
    value integer NOT NULL,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_price_item OWNER TO mcadmin;

--
-- Name: catalog_product; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_product (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(2048),
    service uuid NOT NULL,
    is_active boolean DEFAULT true,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_product OWNER TO mcadmin;

--
-- Name: catalog_product_offering; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_product_offering (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    type character varying(32) NOT NULL,
    product uuid NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(2048),
    is_offered boolean DEFAULT false,
    price uuid,
    offer_term uuid,
    usage_balance_id uuid,
    periodic_balance_id uuid,
    cardinality integer,
    is_deleted boolean DEFAULT false,
    is_primary boolean DEFAULT false NOT NULL,
    is_regular boolean DEFAULT true NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_product_offering OWNER TO mcadmin;

--
-- Name: catalog_provider; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_provider (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_provider OWNER TO mcadmin;

--
-- Name: catalog_provider_service; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_provider_service (
    provider uuid NOT NULL,
    service uuid NOT NULL
);


ALTER TABLE public.catalog_provider_service OWNER TO mcadmin;

--
-- Name: catalog_rating_parameter; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_rating_parameter (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(2048),
    custom_list uuid,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_rating_parameter OWNER TO mcadmin;

--
-- Name: catalog_rating_parameter_set; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_rating_parameter_set (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    description character varying(2048),
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_rating_parameter_set OWNER TO mcadmin;

--
-- Name: catalog_rating_parameter_set_to_rating_parameter; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_rating_parameter_set_to_rating_parameter (
    rating_parameter_set uuid,
    rating_parameter uuid NOT NULL
);


ALTER TABLE public.catalog_rating_parameter_set_to_rating_parameter OWNER TO mcadmin;

--
-- Name: catalog_recurring_type; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_recurring_type (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    type character varying(32) NOT NULL,
    value integer NOT NULL,
    is_prorated boolean DEFAULT false,
    is_calendar boolean DEFAULT true
);


ALTER TABLE public.catalog_recurring_type OWNER TO mcadmin;

--
-- Name: catalog_service; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_service (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_service OWNER TO mcadmin;

--
-- Name: catalog_tariff; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_tariff (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    version bigint,
    name character varying(256) NOT NULL,
    description character varying(2048),
    type character varying(32),
    is_deleted boolean DEFAULT false,
    is_system boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone,
    is_adaptive boolean DEFAULT false NOT NULL
);


ALTER TABLE public.catalog_tariff OWNER TO mcadmin;

--
-- Name: catalog_tariff_plan; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_tariff_plan (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    layout uuid NOT NULL,
    calendar uuid,
    version bigint,
    name character varying(256) NOT NULL,
    description character varying(2048),
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_tariff_plan OWNER TO mcadmin;

--
-- Name: catalog_tariff_plan_to_calendar_range; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_tariff_plan_to_calendar_range (
    catalog_tariff_plan uuid,
    tariff_calendar_range uuid NOT NULL
);


ALTER TABLE public.catalog_tariff_plan_to_calendar_range OWNER TO mcadmin;

--
-- Name: catalog_tariff_step; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_tariff_step (
    id uuid NOT NULL,
    version bigint,
    tariff uuid NOT NULL,
    price_item uuid NOT NULL,
    name character varying(256) NOT NULL,
    step integer NOT NULL,
    value integer DEFAULT 1,
    repetitions integer DEFAULT 1,
    next_step integer,
    is_deleted boolean DEFAULT false,
    currency_id character(3),
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.catalog_tariff_step OWNER TO mcadmin;

--
-- Name: catalog_version; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_version (
    id uuid NOT NULL,
    version bigint NOT NULL,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    is_active boolean DEFAULT false,
    is_deleted boolean DEFAULT false,
    description character varying(2048),
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone
);


ALTER TABLE public.catalog_version OWNER TO mcadmin;

--
-- Name: catalog_version_properties; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.catalog_version_properties (
    version uuid NOT NULL,
    catalog_object character varying(256) NOT NULL,
    reference_object uuid NOT NULL
);


ALTER TABLE public.catalog_version_properties OWNER TO mcadmin;

--
-- Name: compatibility_set; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.compatibility_set (
    id uuid NOT NULL,
    name character varying(256),
    description character varying(2048),
    type character varying(32) NOT NULL,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.compatibility_set OWNER TO mcadmin;

--
-- Name: compatibility_set_to_product_offering; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.compatibility_set_to_product_offering (
    compatibility_set_id uuid NOT NULL,
    product_offering_id uuid NOT NULL
);


ALTER TABLE public.compatibility_set_to_product_offering OWNER TO mcadmin;

--
-- Name: currency; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.currency (
    id character(3) NOT NULL,
    name character varying(256) NOT NULL,
    type character varying(32)
);


ALTER TABLE public.currency OWNER TO mcadmin;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.customer (
    id uuid NOT NULL,
    parent uuid,
    realm character varying(128) NOT NULL,
    status character varying(256),
    is_deleted boolean DEFAULT false,
    path character varying(1024) NOT NULL,
    custom_fields jsonb
);


ALTER TABLE public.customer OWNER TO mcadmin;

--
-- Name: customer_account; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.customer_account (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    version bigint,
    parent uuid,
    customer uuid NOT NULL,
    currency character(3),
    status character varying(256),
    is_deleted boolean DEFAULT false,
    path character varying(1024) NOT NULL,
    custom_fields jsonb,
    billing_cycle_period_type character varying(32),
    billing_cycle_period_day integer,
    last_billing_renewal timestamp without time zone,
    next_billing_renewal timestamp without time zone,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.customer_account OWNER TO mcadmin;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO mcadmin;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO mcadmin;

--
-- Name: eligibility_set; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.eligibility_set (
    id uuid NOT NULL,
    name character varying(256),
    description character varying(2048),
    condition text NOT NULL,
    schema_id uuid NOT NULL,
    is_deleted boolean DEFAULT false,
    expression text,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.eligibility_set OWNER TO mcadmin;

--
-- Name: eligibility_set_schema; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.eligibility_set_schema (
    id uuid NOT NULL,
    schema jsonb NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    is_system boolean DEFAULT false NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.eligibility_set_schema OWNER TO mcadmin;

--
-- Name: exchange_rate; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.exchange_rate (
    id uuid NOT NULL,
    realm character varying(128) NOT NULL,
    provider uuid,
    currency character(3) NOT NULL,
    rate bigint NOT NULL,
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.exchange_rate OWNER TO mcadmin;

--
-- Name: layout_rating_parameters; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.layout_rating_parameters (
    id uuid NOT NULL,
    "position" bigint,
    group_id uuid,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.layout_rating_parameters OWNER TO mcadmin;

--
-- Name: layout_rating_parameters_group; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.layout_rating_parameters_group (
    id uuid NOT NULL,
    name character varying(256) NOT NULL,
    parameters jsonb,
    is_deleted boolean DEFAULT false,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.layout_rating_parameters_group OWNER TO mcadmin;

--
-- Name: layout_rating_parameters_to_parameter; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.layout_rating_parameters_to_parameter (
    layout_rating_parameters uuid NOT NULL,
    parameter character varying(128) NOT NULL
);


ALTER TABLE public.layout_rating_parameters_to_parameter OWNER TO mcadmin;

--
-- Name: layout_tags; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.layout_tags (
    layout_rating_parameters uuid NOT NULL,
    tag character varying(128) NOT NULL
);


ALTER TABLE public.layout_tags OWNER TO mcadmin;

--
-- Name: lcp_status; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.lcp_status (
    status character varying(256) NOT NULL
);


ALTER TABLE public.lcp_status OWNER TO mcadmin;

--
-- Name: lcp_status_service; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.lcp_status_service (
    status character varying(256) NOT NULL,
    service uuid NOT NULL
);


ALTER TABLE public.lcp_status_service OWNER TO mcadmin;

--
-- Name: lookup; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.lookup (
    id uuid NOT NULL,
    module character varying(256),
    key character varying(256) NOT NULL,
    realm character varying(128) NOT NULL,
    value character varying(128) NOT NULL,
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone,
    is_deleted boolean DEFAULT false
);


ALTER TABLE public.lookup OWNER TO mcadmin;

--
-- Name: product_offering_to_compatibility_set; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.product_offering_to_compatibility_set (
    compatibility_set_id uuid NOT NULL,
    product_offering_id uuid NOT NULL
);


ALTER TABLE public.product_offering_to_compatibility_set OWNER TO mcadmin;

--
-- Name: product_offering_to_eligibility_set; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.product_offering_to_eligibility_set (
    eligibility_set_id uuid NOT NULL,
    product_offering_id uuid
);


ALTER TABLE public.product_offering_to_eligibility_set OWNER TO mcadmin;

--
-- Name: rating_offer_details; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.rating_offer_details (
    id uuid NOT NULL,
    subscriber_offer_id uuid,
    product_offering_id uuid NOT NULL,
    service_id uuid NOT NULL,
    product_id uuid NOT NULL,
    tariff_plan_id uuid,
    usage_balance_id uuid,
    periodic_balance_id uuid,
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone,
    priority integer,
    subscriber_id uuid,
    is_primary boolean DEFAULT false
);


ALTER TABLE public.rating_offer_details OWNER TO mcadmin;

--
-- Name: recurring_offer_details; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.recurring_offer_details (
    id uuid NOT NULL,
    subscriber_offer_id uuid,
    product_offering_id uuid NOT NULL,
    service_id uuid NOT NULL,
    product_id uuid NOT NULL,
    price_item_currency character(3),
    price_item_value bigint,
    effective_date_from timestamp without time zone NOT NULL,
    effective_date_to timestamp without time zone,
    usage_balance_id uuid,
    periodic_balance_id uuid,
    allowance integer,
    recurring_type character varying(32) NOT NULL,
    recurring_value integer NOT NULL,
    is_recurring_prorated boolean DEFAULT false NOT NULL,
    is_recurring_calendar boolean DEFAULT false NOT NULL,
    next_charge_date timestamp without time zone,
    last_charge_date timestamp without time zone,
    is_primary boolean,
    priority integer,
    subscriber_id uuid
);


ALTER TABLE public.recurring_offer_details OWNER TO mcadmin;

--
-- Name: subscriber; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.subscriber (
    id uuid NOT NULL,
    version bigint,
    customer_account uuid NOT NULL,
    is_deleted boolean DEFAULT false,
    is_virtual boolean DEFAULT false,
    status character varying(256),
    path text,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.subscriber OWNER TO mcadmin;

--
-- Name: subscriber_balance; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.subscriber_balance (
    id uuid NOT NULL,
    parent uuid,
    type character varying(32) NOT NULL,
    currency character varying(32) NOT NULL,
    balance bigint DEFAULT 0 NOT NULL,
    is_virtual boolean DEFAULT false,
    minimum_balance bigint,
    maximum_balance bigint,
    subscriber uuid,
    subscriber_offer uuid,
    is_deleted boolean DEFAULT false,
    realm character varying(128) NOT NULL,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.subscriber_balance OWNER TO mcadmin;

--
-- Name: subscriber_offer; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.subscriber_offer (
    id uuid NOT NULL,
    description character varying(2048),
    is_primary boolean DEFAULT false,
    is_deleted boolean DEFAULT false,
    subscriber uuid NOT NULL,
    product_offering uuid NOT NULL,
    offer_details jsonb,
    realm character varying(128) NOT NULL,
    name character varying(256) NOT NULL,
    priority integer,
    is_active boolean DEFAULT true NOT NULL,
    activated_at timestamp without time zone,
    created_date timestamp without time zone DEFAULT now() NOT NULL,
    updated_date timestamp without time zone
);


ALTER TABLE public.subscriber_offer OWNER TO mcadmin;

--
-- Name: subscriber_offer_to_subscriber_balance; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.subscriber_offer_to_subscriber_balance (
    subscriber_offer uuid NOT NULL,
    subscriber_balance uuid NOT NULL
);


ALTER TABLE public.subscriber_offer_to_subscriber_balance OWNER TO mcadmin;

--
-- Name: tariff_calendar_range; Type: TABLE; Schema: public; Owner: mcadmin
--

CREATE TABLE public.tariff_calendar_range (
    id uuid NOT NULL,
    "position" bigint,
    tariff_id uuid,
    calendar_properties_id uuid
);


ALTER TABLE public.tariff_calendar_range OWNER TO mcadmin;

--
-- Data for Name: catalog_balance; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_balance (id, realm, description, type, currency, is_virtual, minimum_balance, maximum_balance, is_deleted, name, created_date, updated_date) FROM stdin;
fc58f574-c83e-4513-a475-c7e7062c99ac		\N	PREPAID	USD	t	\N	\N	f	GswTDIHsCx	2020-10-21 11:29:37.305	2020-10-21 11:29:37.305
e8b3b0e0-9830-49b0-b9be-cc0ac32bbcc0		\N	PREPAID	USD	t	\N	\N	f	LADQFalmuY	2020-10-21 11:29:38.092	2020-10-21 11:29:38.092
9d69775c-6426-48bd-a575-a4182ee55014		\N	PREPAID	USD	t	\N	\N	f	cIXALHnBaE	2020-10-21 11:29:38.37	2020-10-21 11:29:38.37
1644cf7d-0b07-4aba-a552-458a17dc1ee9		\N	PREPAID	USD	t	\N	\N	f	QRqpUKEGvo	2020-10-21 11:29:38.67	2020-10-21 11:29:38.67
5480bf17-7b71-442c-956a-69f33083aa93		\N	PREPAID	USD	t	\N	\N	f	NzzXMYlEFc	2020-10-21 11:29:38.96	2020-10-21 11:29:38.96
ca084aab-c77e-47ec-8df0-d7d895cd976a		\N	PREPAID	USD	t	\N	\N	f	nVsxTGNSwb	2020-10-21 11:29:39.247	2020-10-21 11:29:39.247
\.


--
-- Data for Name: catalog_bundle_to_nested_product_offer; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_bundle_to_nested_product_offer (bundle_product_offer, nested_product_offer) FROM stdin;
\.


--
-- Data for Name: catalog_calendar; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_calendar (id, realm, version, name, description, is_deleted, created_date, updated_date) FROM stdin;
ab526740-534b-46da-8c67-e45806a1ee76		0	EAQsvbiMcb	\N	f	2020-10-21 11:29:37.486	2020-10-21 11:29:37.486
bdd1ad62-eb86-463b-854c-2d17105f0576		0	KjvQpMdgki	\N	f	2020-10-21 11:29:38.174	2020-10-21 11:29:38.174
c03fd7b0-1208-443d-af00-223ac1c057c8		0	oAjBMRDWDA	\N	f	2020-10-21 11:29:38.435	2020-10-21 11:29:38.435
bf35817a-2da4-4539-8bd8-9f5e3ccd5cc0		0	DNGRBVoJSx	\N	f	2020-10-21 11:29:38.744	2020-10-21 11:29:38.744
3931675b-a3da-4fce-b54d-d2e247924b16		0	MBcotDPncC	\N	f	2020-10-21 11:29:39.02	2020-10-21 11:29:39.02
c88fe5e3-3aea-48af-a1bf-00ad88630af3		0	zLdlGtbxmT	\N	f	2020-10-21 11:29:39.308	2020-10-21 11:29:39.308
\.


--
-- Data for Name: catalog_calendar_properties; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_calendar_properties (id, calendar, version, name, month_from, month_to, dom_from, dom_to, time_from, time_to, is_deleted, year_from, year_to, dow_from, dow_to, description, color, special, created_date, updated_date) FROM stdin;
622e7537-5a68-4be8-97ce-84c5fc551d01	ab526740-534b-46da-8c67-e45806a1ee76	0	WIvkDTgveL	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:37.583	2020-10-21 11:29:37.583
8c3ba17d-f882-4c76-ad79-a83e48069c36	bdd1ad62-eb86-463b-854c-2d17105f0576	0	IefhKfLqht	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:38.194	2020-10-21 11:29:38.194
108c0f3d-6714-4885-9190-2c9ce42a6189	c03fd7b0-1208-443d-af00-223ac1c057c8	0	WMQFTWAuqU	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:38.457	2020-10-21 11:29:38.457
6ee65672-4590-4e02-a0b4-3012ad211ce4	bf35817a-2da4-4539-8bd8-9f5e3ccd5cc0	0	QINXbmPGGu	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:38.767	2020-10-21 11:29:38.767
c409237e-f3c8-4299-ab42-1ad2fe36bcac	3931675b-a3da-4fce-b54d-d2e247924b16	0	luvfRaBEqL	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:39.04	2020-10-21 11:29:39.04
ea13476e-f579-40c4-a3f0-81f8104518b4	c88fe5e3-3aea-48af-a1bf-00ad88630af3	0	oaLDTQVWWf	1	\N	0	\N	00:00:00	\N	f	2020	\N	0	\N	\N	\N	f	2020-10-21 11:29:39.327	2020-10-21 11:29:39.327
\.


--
-- Data for Name: catalog_custom_list; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_custom_list (id, realm, name, description, list, is_deleted, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: catalog_layout; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_layout (id, realm, version, rating_parameter_set, name, description, data, is_deleted, created_date, updated_date) FROM stdin;
795102fc-6ff2-4c83-913b-9f1b7bb410f2		0	\N	kAxNoNUUme	\N	\N	f	2020-10-21 11:29:37.686	2020-10-21 11:29:37.686
cf1acf64-0706-4e53-b54e-4d2690b8c94e		0	\N	WBvlNKcqPQ	\N	\N	f	2020-10-21 11:29:38.219	2020-10-21 11:29:38.219
54e355e7-c280-4c27-b47e-ed6b5661dc48		0	\N	rJGkOmTPJA	\N	\N	f	2020-10-21 11:29:38.482	2020-10-21 11:29:38.482
78689e95-e5cb-418e-bd54-de0ac0205b20		0	\N	NWVNEGrohw	\N	\N	f	2020-10-21 11:29:38.788	2020-10-21 11:29:38.788
ef0c00cf-c60b-4229-b596-d12bc582d147		0	\N	aZhOorIzmi	\N	\N	f	2020-10-21 11:29:39.057	2020-10-21 11:29:39.057
fba8d724-a04b-4ce1-8e0e-8162184b5fc0		0	\N	rVsePnOtvV	\N	\N	f	2020-10-21 11:29:39.344	2020-10-21 11:29:39.344
\.


--
-- Data for Name: catalog_layout_to_layout_rating_parameters; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_layout_to_layout_rating_parameters (catalog_layout, layout_rating_parameters) FROM stdin;
795102fc-6ff2-4c83-913b-9f1b7bb410f2	f27802b4-c210-4204-90db-31df6146e124
cf1acf64-0706-4e53-b54e-4d2690b8c94e	7fcb7d67-2616-4c7b-9e83-a80269c5551e
54e355e7-c280-4c27-b47e-ed6b5661dc48	d5316ae4-4ae3-4c20-ad85-b8fa8a4c695b
78689e95-e5cb-418e-bd54-de0ac0205b20	f9bfb28c-fe2c-4ed7-9719-2fb75791a88a
ef0c00cf-c60b-4229-b596-d12bc582d147	e3e7a903-8775-4d59-9ebd-3ea80e762aa8
fba8d724-a04b-4ce1-8e0e-8162184b5fc0	b98828f0-3417-47cd-b829-084847f61aae
\.


--
-- Data for Name: catalog_offer_price; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_offer_price (id, realm, currency, value) FROM stdin;
3bc208a6-ddcb-4ec9-b39f-908c6d55245c		USD	0
cc387644-6be2-4085-9cfe-392832f9b5db		USD	0
aa578d54-0ddd-4c0b-93ae-0ddd494c0890		USD	0
494e00dc-89c7-497c-a402-e784197c5fe7		USD	0
4a632554-9044-46ca-b569-bf0c9c89ac50		USD	0
7e8fce5b-3cfc-46df-a91a-7af039cf1f33		USD	0
\.


--
-- Data for Name: catalog_offer_term; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_offer_term (id, realm, type, recurring_type, tariff_plan, offer_priority, allowance, effective_date_from, effective_date_to) FROM stdin;
34f33862-a6b3-435b-96cf-8d330d1daeba		RATING	f10bbc91-df61-49e9-b03c-d71e510d121e	9cb57715-85e8-4325-aee2-4a5804805884	90	0	2020-05-01 03:00:00.999	\N
af94d9d5-8d6c-4ddf-a68a-292cd264dabd		RATING	e9a9a3b4-93c6-494b-9f6e-5362db74783b	f26a3833-a775-47a1-8a70-e79ac07a8f7c	90	0	2020-05-01 03:00:00.999	\N
78632602-5fb0-4dbd-bdae-9c266a24cb02		RATING	cae97ad1-02f9-40bb-9943-c5dd4a2f9a33	69a44bd2-0b19-43d3-935b-70f234366dac	90	0	2020-05-01 03:00:00.999	\N
acc00529-f74c-449a-bc9c-6e81a62870ef		RATING	59442e66-6d76-4c7e-b3c1-3efa4179c672	6468f938-edab-45fd-9160-57095d1da016	90	0	2020-05-01 03:00:00.999	\N
559eed45-68bd-4e21-af94-154b2ccb1018		RATING	23dbe255-a1ce-463a-be3f-da0d9e2ce3fd	ee2284e6-ab2b-456a-9f91-25e3618792a9	90	0	2020-05-01 03:00:00.999	\N
afb7928f-c1e3-49a2-a73f-fb8e8b4c1149		RATING	2c9a3af0-b415-4547-bb84-b2cb7622ea0a	0729c086-60ca-4ece-8255-a6b37adb76f1	90	0	2020-05-01 03:00:00.999	\N
\.


--
-- Data for Name: catalog_price_item; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_price_item (id, realm, version, currency, name, description, value, is_deleted, created_date, updated_date) FROM stdin;
f88665df-9421-406a-a2bd-8ee3a9a39391		0	USD	jopjBARHXI	\N	3	f	2020-10-21 11:29:37.847	\N
d7a89f60-9524-482f-8a54-7bb0f6a4eac0		0	USD	PJyqSFsPHu	\N	5	f	2020-10-21 11:29:38.275	\N
458b0e9d-ee54-4451-b401-08ac287187bb		0	USD	KCUtVqCOfG	\N	5	f	2020-10-21 11:29:38.536	\N
0639b36b-453e-4acf-af89-aad476feeabf		0	USD	QmnDyezZOd	\N	5	f	2020-10-21 11:29:38.565	\N
20a4717d-5994-4061-8245-d57744561cca		0	USD	WEdTItkVhY	\N	3	f	2020-10-21 11:29:38.839	\N
c30a7509-ad68-43fa-bf65-a52e7d839e29		0	USD	WEdTItkVhY	\N	3	f	2020-10-21 11:29:38.869	\N
33d6b206-1fed-407c-8bd0-55bce4e1e4a2		0	USD	oyCNYUBPiv	\N	3	f	2020-10-21 11:29:39.104	\N
4826c0db-7f9e-4c92-bb88-7a85d316602d		0	USD	ZlBvUnfwpW	\N	4	f	2020-10-21 11:29:39.135	\N
d8b1cf0b-3089-4834-8878-5fa0f8f94e21		0	USD	qZBZhuhnHX	\N	5	f	2020-10-21 11:29:39.165	\N
eb926ba0-99eb-4398-a585-6a29dd44f986		0	USD	eHrqQnZwzR	\N	3	f	2020-10-21 11:29:39.396	\N
78a5793e-6f81-4832-a751-a68cdfaae3fc		0	USD	yxrKCqyDvH	\N	4	f	2020-10-21 11:29:39.424	\N
3c70ef9d-273a-4e2a-8cc0-9edfc87e065c		0	USD	iWgmODhpaR	\N	5	f	2020-10-21 11:29:39.448	\N
\.


--
-- Data for Name: catalog_product; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_product (id, realm, name, description, service, is_active, is_deleted, created_date, updated_date) FROM stdin;
a4cef599-0e68-4b53-b6b2-21df701dbe2d		SMS hDYIx	\N	543a5c91-708f-49f1-b033-6b56e811f208	t	f	2020-10-21 11:29:37.453	2020-10-21 11:29:37.453
59af223e-b15f-44b5-a4b3-952d63fd271d		SMS Sgczt	\N	7b7bdfad-0326-40d1-9959-9ba15b8d77d0	t	f	2020-10-21 11:29:38.158	2020-10-21 11:29:38.158
9b2f2c85-0148-4258-afb8-17fb837c155f		VoiceCall ZUfyH	\N	9327d8da-ff32-48ef-85da-09910d12a8ae	t	f	2020-10-21 11:29:38.42	2020-10-21 11:29:38.42
ce33054c-ba74-4df4-b1b9-c34378cff0b1		VoiceCall  XyoOS	\N	8cdb43c7-309f-4ae1-b291-cbecb13b6680	t	f	2020-10-21 11:29:38.722	2020-10-21 11:29:38.722
db5c02d0-b5f7-4577-8c75-80280e0a9e43		Internet Psvgu	\N	a5628523-91f0-4534-a704-4af62ae768d5	t	f	2020-10-21 11:29:39.005	2020-10-21 11:29:39.005
61e86792-dabe-4e1c-ba89-9e924e03886f		Internet NHOsA	\N	59651437-aa0f-47c4-aa9b-5d14be35350a	t	f	2020-10-21 11:29:39.295	2020-10-21 11:29:39.295
\.


--
-- Data for Name: catalog_product_offering; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_product_offering (id, realm, type, product, name, description, is_offered, price, offer_term, usage_balance_id, periodic_balance_id, cardinality, is_deleted, is_primary, is_regular, created_date, updated_date) FROM stdin;
f38bf460-beef-4d81-812c-8a3276446bda		SIMPLE_PRODUCT_OFFERING	a4cef599-0e68-4b53-b6b2-21df701dbe2d	SMS hDYIx	\N	t	3bc208a6-ddcb-4ec9-b39f-908c6d55245c	34f33862-a6b3-435b-96cf-8d330d1daeba	fc58f574-c83e-4513-a475-c7e7062c99ac	fc58f574-c83e-4513-a475-c7e7062c99ac	5	f	t	t	2020-10-21 11:29:38.034	2020-10-21 11:29:38.034
206aabf0-6c43-453a-ac14-aebaf80836f4		SIMPLE_PRODUCT_OFFERING	59af223e-b15f-44b5-a4b3-952d63fd271d	SMS Sgczt	\N	t	cc387644-6be2-4085-9cfe-392832f9b5db	af94d9d5-8d6c-4ddf-a68a-292cd264dabd	e8b3b0e0-9830-49b0-b9be-cc0ac32bbcc0	e8b3b0e0-9830-49b0-b9be-cc0ac32bbcc0	5	f	t	t	2020-10-21 11:29:38.345	2020-10-21 11:29:38.345
dd79745c-1004-4acd-bf5d-f8b2c83f5605		SIMPLE_PRODUCT_OFFERING	9b2f2c85-0148-4258-afb8-17fb837c155f	VoiceCall ZUfyH	\N	t	aa578d54-0ddd-4c0b-93ae-0ddd494c0890	78632602-5fb0-4dbd-bdae-9c266a24cb02	9d69775c-6426-48bd-a575-a4182ee55014	9d69775c-6426-48bd-a575-a4182ee55014	5	f	t	t	2020-10-21 11:29:38.644	2020-10-21 11:29:38.644
7a7ca946-f1d0-457e-a874-9965e2ed6448		SIMPLE_PRODUCT_OFFERING	ce33054c-ba74-4df4-b1b9-c34378cff0b1	VoiceCall  XyoOS	\N	t	494e00dc-89c7-497c-a402-e784197c5fe7	acc00529-f74c-449a-bc9c-6e81a62870ef	1644cf7d-0b07-4aba-a552-458a17dc1ee9	1644cf7d-0b07-4aba-a552-458a17dc1ee9	5	f	t	t	2020-10-21 11:29:38.933	2020-10-21 11:29:38.933
a2105551-f4f4-4a75-a52f-1112452ba300		SIMPLE_PRODUCT_OFFERING	db5c02d0-b5f7-4577-8c75-80280e0a9e43	Internet Psvgu	\N	t	4a632554-9044-46ca-b569-bf0c9c89ac50	559eed45-68bd-4e21-af94-154b2ccb1018	5480bf17-7b71-442c-956a-69f33083aa93	5480bf17-7b71-442c-956a-69f33083aa93	5	f	t	t	2020-10-21 11:29:39.219	2020-10-21 11:29:39.219
44d8dfdb-ab8c-4b14-8fec-6fead08aed08		SIMPLE_PRODUCT_OFFERING	61e86792-dabe-4e1c-ba89-9e924e03886f	Internet NHOsA	\N	t	7e8fce5b-3cfc-46df-a91a-7af039cf1f33	afb7928f-c1e3-49a2-a73f-fb8e8b4c1149	ca084aab-c77e-47ec-8df0-d7d895cd976a	ca084aab-c77e-47ec-8df0-d7d895cd976a	5	f	t	t	2020-10-21 11:29:39.506	2020-10-21 11:29:39.506
\.


--
-- Data for Name: catalog_provider; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_provider (id, realm, name, is_deleted, created_date, updated_date) FROM stdin;
0a445e3f-e3e1-4e46-8e84-3e0269e3edd4		SMS hDYIx	f	2020-10-21 11:29:37.405	2020-10-21 11:29:37.405
e5227faa-a08e-43a1-ad3a-5146d253d739		SMS Sgczt	f	2020-10-21 11:29:38.137	2020-10-21 11:29:38.137
5325f4bc-bd33-4bb3-a8dd-eabff23265d8		VoiceCall ZUfyH	f	2020-10-21 11:29:38.402	2020-10-21 11:29:38.402
cb5b85fd-420b-4dd2-adab-b463ace9af90		VoiceCall  XyoOS	f	2020-10-21 11:29:38.704	2020-10-21 11:29:38.704
30c6d99e-6346-4225-87e9-da7a411e0653		Internet Psvgu	f	2020-10-21 11:29:38.987	2020-10-21 11:29:38.987
48c74e9b-0390-45aa-9bc3-f06d93b61bb3		Internet NHOsA	f	2020-10-21 11:29:39.278	2020-10-21 11:29:39.278
\.


--
-- Data for Name: catalog_provider_service; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_provider_service (provider, service) FROM stdin;
0a445e3f-e3e1-4e46-8e84-3e0269e3edd4	543a5c91-708f-49f1-b033-6b56e811f208
e5227faa-a08e-43a1-ad3a-5146d253d739	7b7bdfad-0326-40d1-9959-9ba15b8d77d0
5325f4bc-bd33-4bb3-a8dd-eabff23265d8	9327d8da-ff32-48ef-85da-09910d12a8ae
cb5b85fd-420b-4dd2-adab-b463ace9af90	8cdb43c7-309f-4ae1-b291-cbecb13b6680
30c6d99e-6346-4225-87e9-da7a411e0653	a5628523-91f0-4534-a704-4af62ae768d5
48c74e9b-0390-45aa-9bc3-f06d93b61bb3	59651437-aa0f-47c4-aa9b-5d14be35350a
\.


--
-- Data for Name: catalog_rating_parameter; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_rating_parameter (id, realm, name, description, custom_list, is_deleted, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: catalog_rating_parameter_set; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_rating_parameter_set (id, realm, name, description, is_deleted, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: catalog_rating_parameter_set_to_rating_parameter; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_rating_parameter_set_to_rating_parameter (rating_parameter_set, rating_parameter) FROM stdin;
\.


--
-- Data for Name: catalog_recurring_type; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_recurring_type (id, realm, type, value, is_prorated, is_calendar) FROM stdin;
f10bbc91-df61-49e9-b03c-d71e510d121e		ONE_TIME	0	f	t
e9a9a3b4-93c6-494b-9f6e-5362db74783b		ONE_TIME	0	f	t
cae97ad1-02f9-40bb-9943-c5dd4a2f9a33		ONE_TIME	0	f	t
59442e66-6d76-4c7e-b3c1-3efa4179c672		ONE_TIME	0	f	t
23dbe255-a1ce-463a-be3f-da0d9e2ce3fd		ONE_TIME	0	f	t
2c9a3af0-b415-4547-bb84-b2cb7622ea0a		ONE_TIME	0	f	t
\.


--
-- Data for Name: catalog_service; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_service (id, realm, name, is_deleted, created_date, updated_date) FROM stdin;
543a5c91-708f-49f1-b033-6b56e811f208		SMS hDYIx	f	2020-10-21 11:29:37.34	2020-10-21 11:29:37.34
7b7bdfad-0326-40d1-9959-9ba15b8d77d0		SMS Sgczt	f	2020-10-21 11:29:38.113	2020-10-21 11:29:38.113
9327d8da-ff32-48ef-85da-09910d12a8ae		VoiceCall ZUfyH	f	2020-10-21 11:29:38.387	2020-10-21 11:29:38.387
8cdb43c7-309f-4ae1-b291-cbecb13b6680		VoiceCall  XyoOS	f	2020-10-21 11:29:38.687	2020-10-21 11:29:38.687
a5628523-91f0-4534-a704-4af62ae768d5		Internet Psvgu	f	2020-10-21 11:29:38.974	2020-10-21 11:29:38.974
59651437-aa0f-47c4-aa9b-5d14be35350a		Internet NHOsA	f	2020-10-21 11:29:39.263	2020-10-21 11:29:39.263
\.


--
-- Data for Name: catalog_tariff; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_tariff (id, realm, version, name, description, type, is_deleted, is_system, created_date, updated_date, is_adaptive) FROM stdin;
95aba16a-ba37-4bf1-a557-e9a9587001ca		0	free	\N	FREE	f	t	2020-10-21 11:28:55.632659	\N	f
8c79d49c-1563-4085-8e57-96df1d917796		0	pass	\N	PASS	f	t	2020-10-21 11:28:55.632659	\N	f
e76bc150-dbde-463d-9702-33257cb08fd0		0	block	\N	BLOCK	f	t	2020-10-21 11:28:55.632659	\N	f
060f8fd6-28e6-49de-a829-ce7ba448173f		0	parent rollover	\N	PARENT_ROLLOVER	f	t	2020-10-21 11:28:55.632659	\N	f
d74a220d-0daf-4c5c-884c-b00e2546fc48		0	account rollover	\N	ACCOUNT_ROLLOVER	f	t	2020-10-21 11:28:55.632659	\N	f
ecbbd7bd-da5f-4aa2-a079-0addac827973		0	SMS hDYIx	\N	REGULAR	f	f	2020-10-21 11:29:37.798	2020-10-21 11:29:37.798	f
3d4de345-63ee-42a7-994c-dc4e2adf2744		0	SMS Sgczt	\N	REGULAR	f	f	2020-10-21 11:29:38.256	2020-10-21 11:29:38.256	f
6ecee306-e81a-46a2-9088-d755c3dce962		0	VoiceCall ZUfyH	\N	REGULAR	f	f	2020-10-21 11:29:38.516	2020-10-21 11:29:38.516	f
b6f42738-b2bc-4ac9-95cb-7da5624113d6		0	VoiceCall  XyoOS	\N	REGULAR	f	f	2020-10-21 11:29:38.821	2020-10-21 11:29:38.821	f
6af3ef92-9c6c-483f-a894-b3ce559bb434		0	Internet Psvgu	\N	REGULAR	f	f	2020-10-21 11:29:39.088	2020-10-21 11:29:39.088	t
6c0dd2e5-fb84-42a0-a691-10c3b1f7e2c0		0	Internet NHOsA	\N	REGULAR	f	f	2020-10-21 11:29:39.374	2020-10-21 11:29:39.374	f
\.


--
-- Data for Name: catalog_tariff_plan; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_tariff_plan (id, realm, layout, calendar, version, name, description, is_deleted, created_date, updated_date) FROM stdin;
9cb57715-85e8-4325-aee2-4a5804805884		795102fc-6ff2-4c83-913b-9f1b7bb410f2	ab526740-534b-46da-8c67-e45806a1ee76	0	SMS hDYIx	\N	f	2020-10-21 11:29:37.926	2020-10-21 11:29:37.926
f26a3833-a775-47a1-8a70-e79ac07a8f7c		cf1acf64-0706-4e53-b54e-4d2690b8c94e	bdd1ad62-eb86-463b-854c-2d17105f0576	0	SMS Sgczt	\N	f	2020-10-21 11:29:38.306	2020-10-21 11:29:38.306
69a44bd2-0b19-43d3-935b-70f234366dac		54e355e7-c280-4c27-b47e-ed6b5661dc48	c03fd7b0-1208-443d-af00-223ac1c057c8	0	VoiceCall ZUfyH	\N	f	2020-10-21 11:29:38.592	2020-10-21 11:29:38.592
6468f938-edab-45fd-9160-57095d1da016		78689e95-e5cb-418e-bd54-de0ac0205b20	bf35817a-2da4-4539-8bd8-9f5e3ccd5cc0	0	VoiceCall  XyoOS	\N	f	2020-10-21 11:29:38.898	2020-10-21 11:29:38.898
ee2284e6-ab2b-456a-9f91-25e3618792a9		ef0c00cf-c60b-4229-b596-d12bc582d147	3931675b-a3da-4fce-b54d-d2e247924b16	0	Internet Psvgu	\N	f	2020-10-21 11:29:39.187	2020-10-21 11:29:39.187
0729c086-60ca-4ece-8255-a6b37adb76f1		fba8d724-a04b-4ce1-8e0e-8162184b5fc0	c88fe5e3-3aea-48af-a1bf-00ad88630af3	0	Internet NHOsA	\N	f	2020-10-21 11:29:39.472	2020-10-21 11:29:39.472
\.


--
-- Data for Name: catalog_tariff_plan_to_calendar_range; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_tariff_plan_to_calendar_range (catalog_tariff_plan, tariff_calendar_range) FROM stdin;
9cb57715-85e8-4325-aee2-4a5804805884	6edbe659-1e68-4510-befa-eaada22bd45f
f26a3833-a775-47a1-8a70-e79ac07a8f7c	a0e202e9-f68e-4f4c-b8ad-14eab13714e9
69a44bd2-0b19-43d3-935b-70f234366dac	bb10e581-54c7-4cc2-899b-8ff2ebb408d3
6468f938-edab-45fd-9160-57095d1da016	c5a60fc5-669b-4ecc-80c9-c36943ecccdf
ee2284e6-ab2b-456a-9f91-25e3618792a9	43f9e252-52fa-4595-8a46-0101c0fe43a5
0729c086-60ca-4ece-8255-a6b37adb76f1	db587c71-b199-4830-8836-9f387a7bb72c
\.


--
-- Data for Name: catalog_tariff_step; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_tariff_step (id, version, tariff, price_item, name, step, value, repetitions, next_step, is_deleted, currency_id, created_date, updated_date) FROM stdin;
0d9c5940-3e8b-4a05-8316-c46dbdb0cb1b	0	ecbbd7bd-da5f-4aa2-a079-0addac827973	f88665df-9421-406a-a2bd-8ee3a9a39391	SMS hDYIx	1	1	\N	\N	f	SMS	2020-10-21 11:29:37.864	2020-10-21 11:29:37.864
70e6708b-c8e4-4186-bf2e-92d8b17f2533	0	3d4de345-63ee-42a7-994c-dc4e2adf2744	d7a89f60-9524-482f-8a54-7bb0f6a4eac0	SMS Sgczt	1	1	\N	\N	f	SMS	2020-10-21 11:29:38.282	2020-10-21 11:29:38.282
0c2cdea6-feba-4489-97c7-d54defa82b4a	0	6ecee306-e81a-46a2-9088-d755c3dce962	458b0e9d-ee54-4451-b401-08ac287187bb	VoiceCall ZUfyH	1	5	2	2	f	SEC	2020-10-21 11:29:38.542	2020-10-21 11:29:38.542
74301dcf-5683-44eb-9843-e960c1008cba	0	6ecee306-e81a-46a2-9088-d755c3dce962	0639b36b-453e-4acf-af89-aad476feeabf	VoiceCall ZUfyH	2	1	\N	\N	f	MIN	2020-10-21 11:29:38.572	2020-10-21 11:29:38.572
160e908c-f94c-4802-bd98-f155f7370731	0	b6f42738-b2bc-4ac9-95cb-7da5624113d6	20a4717d-5994-4061-8245-d57744561cca	VoiceCall  XyoOS	1	10	1	2	f	SEC	2020-10-21 11:29:38.845	2020-10-21 11:29:38.845
6c17d40d-f844-44d2-b3a8-06310abaa7d5	0	b6f42738-b2bc-4ac9-95cb-7da5624113d6	c30a7509-ad68-43fa-bf65-a52e7d839e29	VoiceCall  XyoOS	2	10	\N	\N	f	SEC	2020-10-21 11:29:38.879	2020-10-21 11:29:38.879
2d1d3621-d8dc-43bd-8fd4-2074f3ebfd73	0	6af3ef92-9c6c-483f-a894-b3ce559bb434	33d6b206-1fed-407c-8bd0-55bce4e1e4a2	Internet Psvgu	1	1	1	2	f	MB 	2020-10-21 11:29:39.11	2020-10-21 11:29:39.11
d30319eb-0580-4c9d-85f7-7a82e755395c	0	6af3ef92-9c6c-483f-a894-b3ce559bb434	4826c0db-7f9e-4c92-bb88-7a85d316602d	Internet Psvgu	2	2	2	3	f	MB 	2020-10-21 11:29:39.143	2020-10-21 11:29:39.143
25659081-c8f9-4533-ba67-08bd57885e0f	0	6af3ef92-9c6c-483f-a894-b3ce559bb434	d8b1cf0b-3089-4834-8878-5fa0f8f94e21	Internet Psvgu	3	1024	\N	\N	f	KB 	2020-10-21 11:29:39.171	2020-10-21 11:29:39.171
1ff9e8d7-94c1-41da-a197-142d26f9e397	0	6c0dd2e5-fb84-42a0-a691-10c3b1f7e2c0	eb926ba0-99eb-4398-a585-6a29dd44f986	Internet NHOsA	1	1	1	2	f	MB 	2020-10-21 11:29:39.403	2020-10-21 11:29:39.403
d4368347-4dc2-47f9-8341-b50cec7f59a4	0	6c0dd2e5-fb84-42a0-a691-10c3b1f7e2c0	78a5793e-6f81-4832-a751-a68cdfaae3fc	Internet NHOsA	2	2	2	3	f	MB 	2020-10-21 11:29:39.43	2020-10-21 11:29:39.43
464cdd79-8e29-4dd1-bed6-bf51d1aef68a	0	6c0dd2e5-fb84-42a0-a691-10c3b1f7e2c0	3c70ef9d-273a-4e2a-8cc0-9edfc87e065c	Internet NHOsA	3	1024	\N	\N	f	KB 	2020-10-21 11:29:39.454	2020-10-21 11:29:39.454
\.


--
-- Data for Name: catalog_version; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_version (id, version, realm, name, is_active, is_deleted, description, effective_date_from, effective_date_to) FROM stdin;
\.


--
-- Data for Name: catalog_version_properties; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.catalog_version_properties (version, catalog_object, reference_object) FROM stdin;
\.


--
-- Data for Name: compatibility_set; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.compatibility_set (id, name, description, type, is_deleted, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: compatibility_set_to_product_offering; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.compatibility_set_to_product_offering (compatibility_set_id, product_offering_id) FROM stdin;
\.


--
-- Data for Name: currency; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.currency (id, name, type) FROM stdin;
MIN	minutes	DURATION
SEC	seconds	DURATION
SMS	Short Message Service	UNIT
GB 	Gigabyte	UNIT
MB 	Megabyte	UNIT
USD	Dollar	MONETARY
KB 	Kilobyte	UNIT
EUR	Euro	MONETARY
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.customer (id, parent, realm, status, is_deleted, path, custom_fields) FROM stdin;
6d209dcc-2b9e-4688-8d38-b4a3c9be38ba	\N		\N	f	accountPath	\N
\.


--
-- Data for Name: customer_account; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.customer_account (id, realm, version, parent, customer, currency, status, is_deleted, path, custom_fields, billing_cycle_period_type, billing_cycle_period_day, last_billing_renewal, next_billing_renewal, created_date, updated_date) FROM stdin;
402bf54d-dbba-4d3c-a826-9b6aec347c2c		0	\N	6d209dcc-2b9e-4688-8d38-b4a3c9be38ba	USD	\N	f	accountPath	\N	\N	\N	\N	\N	2020-10-21 11:29:36.717	2020-10-21 11:29:36.717
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
lookup	danielzadorozhniy	db/changelog/lookup_entity.xml	2020-10-21 11:28:48.422513	1	EXECUTED	8:68604fcbb3ecb82c2af48c71a66d6bf8	createTable tableName=lookup		\N	3.8.9	\N	\N	3268928345
lookup_constraint	danielzadorozhniy	db/changelog/lookup_constraint.xml	2020-10-21 11:28:48.707133	2	EXECUTED	8:1d489e195632be8ebf9a12fe7040b583	addUniqueConstraint constraintName=lookup_unq01, tableName=lookup		\N	3.8.9	\N	\N	3268928654
1584954807902-69	dkorniichuk	db/changelog/lookup_add_is_deleted_column.xml	2020-10-21 11:28:48.979269	3	EXECUTED	8:a8dbbb23f0b74e8ee9c0d32c3490e766	addColumn tableName=lookup		\N	3.8.9	\N	\N	3268928920
1584944807902-1	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_balance_entity.xml	2020-10-21 11:28:50.531429	4	EXECUTED	8:1bdb70ae793b5dab1494c0b134cb9f97	createTable tableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-2	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_entity.xml	2020-10-21 11:28:50.551207	5	EXECUTED	8:bda53b4310f382ffdf6bf6a86c9d7019	createTable tableName=catalog_rating_parameter		\N	3.8.9	\N	\N	3268930328
1584944807902-3	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_set_entity.xml	2020-10-21 11:28:50.570558	6	EXECUTED	8:642542ae3ca2cde1dbd05a1dbcba6ee3	createTable tableName=catalog_rating_parameter_set		\N	3.8.9	\N	\N	3268930328
1584944807902-4	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_set_to_rating_parameter_entity.xml	2020-10-21 11:28:50.581768	7	EXECUTED	8:66a6812e592606410c6e570f4f9337e7	createTable tableName=catalog_rating_parameter_set_to_rating_parameter		\N	3.8.9	\N	\N	3268930328
1584944807902-5	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_bundle_to_simple_product_offer_entity.xml	2020-10-21 11:28:50.595741	8	EXECUTED	8:5df0d37b2c39832ec9fd83663d59ea22	createTable tableName=catalog_bundle_to_nested_product_offer		\N	3.8.9	\N	\N	3268930328
1584944807902-6	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_calendar_entity.xml	2020-10-21 11:28:50.61294	9	EXECUTED	8:70ce93cfee292e54af62b8df96978351	createTable tableName=catalog_calendar		\N	3.8.9	\N	\N	3268930328
1584944807902-7	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_calendar_properties_entity.xml	2020-10-21 11:28:50.629656	10	EXECUTED	8:4a3d4032c451791dc3291413626ae435	createTable tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
1584944807902-8	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_custom_list_entity.xml	2020-10-21 11:28:50.645994	11	EXECUTED	8:aa7456f0533512e315ec5b328fb87d52	createTable tableName=catalog_custom_list		\N	3.8.9	\N	\N	3268930328
1584944807902-9	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_layout_entity.xml	2020-10-21 11:28:50.661537	12	EXECUTED	8:7ed2f8feda1869fc769e1c41b1c01871	createTable tableName=catalog_layout		\N	3.8.9	\N	\N	3268930328
1584944807902-10	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_offer_price_entity.xml	2020-10-21 11:28:50.675344	13	EXECUTED	8:cd75dce798d9c1ca6de6cce824dbc84c	createTable tableName=catalog_offer_price		\N	3.8.9	\N	\N	3268930328
1584944807902-11	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_offer_term_entity.xml	2020-10-21 11:28:50.691817	14	EXECUTED	8:be1b8cfb4a542dbd389dbb85a201b9f6	createTable tableName=catalog_offer_term		\N	3.8.9	\N	\N	3268930328
1584944807902-12	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_price_item_entity.xml	2020-10-21 11:28:50.709403	15	EXECUTED	8:4b447e05ca18da590d7baa56aae908a2	createTable tableName=catalog_price_item		\N	3.8.9	\N	\N	3268930328
1584944807902-13	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_entity.xml	2020-10-21 11:28:50.72751	16	EXECUTED	8:a83e7fbf0ef46df0bdbe46342c6c581f	createTable tableName=catalog_product		\N	3.8.9	\N	\N	3268930328
1584944807902-14	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_entity.xml	2020-10-21 11:28:50.745189	17	EXECUTED	8:737ec4ff5fd903a6d4f5f37c941143a0	createTable tableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
1584944807902-15	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_provider_entity.xml	2020-10-21 11:28:50.760474	18	EXECUTED	8:40a37550549ced9ee62d2ccf2e2aa87b	createTable tableName=catalog_provider		\N	3.8.9	\N	\N	3268930328
1584944807902-16	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_provider_service_entity.xml	2020-10-21 11:28:50.775351	19	EXECUTED	8:636dbae8636baf04872fbf247c433051	createTable tableName=catalog_provider_service		\N	3.8.9	\N	\N	3268930328
1584944807902-17	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_recurring_type_entity.xml	2020-10-21 11:28:50.790597	20	EXECUTED	8:f5e5bd773bc096faa2ab0dfd1cd60ba4	createTable tableName=catalog_recurring_type		\N	3.8.9	\N	\N	3268930328
1584944807902-18	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_service_entity.xml	2020-10-21 11:28:50.804576	21	EXECUTED	8:f1e46f1565efb4383c26731a48e6d13a	createTable tableName=catalog_service		\N	3.8.9	\N	\N	3268930328
1584944807902-19	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_entity.xml	2020-10-21 11:28:50.82335	22	EXECUTED	8:ea47be6d190934209f247e34c875a440	createTable tableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
1584944807902-20	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_plan_entity.xml	2020-10-21 11:28:50.844429	23	EXECUTED	8:6fdcc0c78e157edec7de049f226fd5f9	createTable tableName=catalog_tariff_plan		\N	3.8.9	\N	\N	3268930328
1584944807902-21	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_step_entity.xml	2020-10-21 11:28:50.859302	24	EXECUTED	8:cb756f62fff3c7028093aa1e8aaf5966	createTable tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
add_column_type_to_catalog_tariff_step	alexk	classpath:db/changelog/202003231650_catalog_tariff_step_entity.xml	2020-10-21 11:28:50.87457	25	EXECUTED	8:e315bd33e68c879d427211d75a081a86	addColumn tableName=catalog_tariff_step; renameColumn newColumnName=value, oldColumnName=quantity, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
1584944807902-22	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_version_entity.xml	2020-10-21 11:28:50.891253	26	EXECUTED	8:19d82b1b88e38a24b98ee74882c4824c	createTable tableName=catalog_version		\N	3.8.9	\N	\N	3268930328
1584944807902-23	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_version_properties_entity.xml	2020-10-21 11:28:50.90852	27	EXECUTED	8:10ca347daf20b846f27090f3e8a4dd42	createTable tableName=catalog_version_properties		\N	3.8.9	\N	\N	3268930328
1584944807902-24	danielzadorozhniy	classpath:db/changelog/202003231650_currency_entity.xml	2020-10-21 11:28:50.92364	28	EXECUTED	8:03570bd14f4a65b6ea704fa1c2d7e14b	createTable tableName=currency		\N	3.8.9	\N	\N	3268930328
1584944807902-25	danielzadorozhniy	classpath:db/changelog/202003231650_customer_entity.xml	2020-10-21 11:28:50.944159	29	EXECUTED	8:b10984fc321fe9beb6902503d1421761	createTable tableName=customer		\N	3.8.9	\N	\N	3268930328
1584944807902-26	danielzadorozhniy	classpath:db/changelog/202003231650_customer_account_entity.xml	2020-10-21 11:28:50.965594	30	EXECUTED	8:27aab77f3c556e2bd38e11a479a8d886	createTable tableName=customer_account		\N	3.8.9	\N	\N	3268930328
1584944807902-27	danielzadorozhniy	classpath:db/changelog/202003231650_exchange_rate_entity.xml	2020-10-21 11:28:50.981027	31	EXECUTED	8:4167018a4ba789708ddfb3714f1eb570	createTable tableName=exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807902-28	danielzadorozhniy	classpath:db/changelog/202003231650_lcp_status_entity.xml	2020-10-21 11:28:50.99515	32	EXECUTED	8:1c52bd214a5342cdaae582a18142a738	createTable tableName=lcp_status		\N	3.8.9	\N	\N	3268930328
1584944807902-29	danielzadorozhniy	classpath:db/changelog/202003231650_lcp_status_service_entity.xml	2020-10-21 11:28:51.010391	33	EXECUTED	8:c0379f5f0f7ae7366a62ed626bbf8b0c	createTable tableName=lcp_status_service		\N	3.8.9	\N	\N	3268930328
1584944807902-31	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_entity.xml	2020-10-21 11:28:51.02465	34	EXECUTED	8:463cdbbad18068c50992da09eb68b6ad	createTable tableName=subscriber		\N	3.8.9	\N	\N	3268930328
1584944807902-32	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_balance_entity.xml	2020-10-21 11:28:51.039784	35	EXECUTED	8:33b77c659821ef5cced03a3ab216eb53	createTable tableName=subscriber_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-33	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_offer_entity.xml	2020-10-21 11:28:51.056094	36	EXECUTED	8:4ecb5b9ad9846baa89aa6f279d5afb86	createTable tableName=subscriber_offer		\N	3.8.9	\N	\N	3268930328
1584944807902-34	danielzadorozhniy	classpath:db/changelog/202003231650_unit_exchange_rate_entity.xml	2020-10-21 11:28:51.076504	37	EXECUTED	8:b893ffe4f5a54ef0212bc2ef8ec89cc4	createTable tableName=unit_exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807902-69	dmytrokorniichuk	classpath:db/changelog/202003231650_catalog_add_is_deleted_column.xml	2020-10-21 11:28:51.126681	38	EXECUTED	8:441041e9c3e59e52d93ac7a89e1061b5	addColumn tableName=catalog_provider; addColumn tableName=catalog_product_offering; addColumn tableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-75	dmytrokorniichuk	classpath:db/changelog/202003231650_catalog_add_is_deleted_column_for_new_entities.xml	2020-10-21 11:28:51.183741	39	EXECUTED	8:e9034d80ffce59fab416ab130a8e3aa2	addColumn tableName=catalog_calendar; addColumn tableName=catalog_custom_list; addColumn tableName=catalog_layout; addColumn tableName=catalog_price_item; addColumn tableName=catalog_rating_parameter; addColumn tableName=catalog_rating_parameter_s...		\N	3.8.9	\N	\N	3268930328
1584944807902-87	dkorniichuk	classpath:db/changelog/202003231650_subscriber_offer_to_subscriber_balance.xml	2020-10-21 11:28:51.205237	40	EXECUTED	8:655964e9031998eb3358854b8f0d8ffa	createTable tableName=subscriber_offer_to_subscriber_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-76	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.232474	41	EXECUTED	8:89d97b7c0faaf325fff04fa31cbc9678	createTable tableName=tariff_calendar_range		\N	3.8.9	\N	\N	3268930328
1584944807902-77	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.250624	42	EXECUTED	8:364a8b1bdb15a8d0a3a1c1473ed80904	addForeignKeyConstraint baseTableName=tariff_calendar_range, constraintName=catalog_tariff_calendar_range_catalog_tariff_fkey, referencedTableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
1584944807902-78	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.267963	43	EXECUTED	8:405b6e59d340d8980ec484d4b14cb164	addForeignKeyConstraint baseTableName=tariff_calendar_range, constraintName=catalog_tariff_calendar_range_catalog_calendar_properties_fkey, referencedTableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
1584944807902-79	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.293027	44	EXECUTED	8:685e3e6d4cce313b1ed685d1ec109607	dropColumn columnName=data, tableName=catalog_tariff_plan		\N	3.8.9	\N	\N	3268930328
1584944807902-80	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.308284	45	EXECUTED	8:8f0140f8c3c3bb3736c5b3b577502dac	createTable tableName=catalog_tariff_plan_to_calendar_range		\N	3.8.9	\N	\N	3268930328
1584944807902-81	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.323583	46	EXECUTED	8:580b7517f92928d30ba1d79878f17d5d	addUniqueConstraint constraintName=catalog_tariff_plan_to_tariff_calendar_range_uk01, tableName=catalog_tariff_plan_to_calendar_range		\N	3.8.9	\N	\N	3268930328
1584944807902-82	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.337229	47	EXECUTED	8:3ceb14bd672241ef261c30e7b608774d	addForeignKeyConstraint baseTableName=catalog_tariff_plan_to_calendar_range, constraintName=catalog_tariff_plan_fkey, referencedTableName=catalog_tariff_plan		\N	3.8.9	\N	\N	3268930328
1584944807902-83	alexkosholap	classpath:db/changelog/202003231650_catalog_tariff_calendar_range.xml	2020-10-21 11:28:51.353224	48	EXECUTED	8:d2cc445b6ca7b8c006f70c07d652e423	addForeignKeyConstraint baseTableName=catalog_tariff_plan_to_calendar_range, constraintName=tariff_calendar_range_fkey, referencedTableName=tariff_calendar_range		\N	3.8.9	\N	\N	3268930328
1584944807902-84	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.369574	49	EXECUTED	8:48277cd4d4f1d026f9305884ad8490c7	createTable tableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
1584944807902-85	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.38234	50	EXECUTED	8:91de466180630e2ab541dc5715297098	createTable tableName=catalog_layout_to_layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
1584944807902-86	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.398374	51	EXECUTED	8:5893e12fd9a2c646a6b91d8cc3c36e6d	createTable tableName=layout_rating_parameters_to_parameter		\N	3.8.9	\N	\N	3268930328
1584944807902-87	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.422422	52	EXECUTED	8:d3fca9e87a11c122b232580360c5e4d5	addUniqueConstraint constraintName=catalog_layout_to_layout_rating_parameters_uk01, tableName=catalog_layout_to_layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
1584944807902-88	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.436302	53	EXECUTED	8:c95e51d68aa31f9d84fcafdf06f55fc3	addForeignKeyConstraint baseTableName=catalog_layout_to_layout_rating_parameters, constraintName=catalog_layout_fkey, referencedTableName=catalog_layout		\N	3.8.9	\N	\N	3268930328
1584944807902-89	alexkosholap	classpath:db/changelog/202003231650_layout_rating_parameters.xml	2020-10-21 11:28:51.4502	54	EXECUTED	8:de8a5469bb47639acffeff61eba990a9	addForeignKeyConstraint baseTableName=catalog_layout_to_layout_rating_parameters, constraintName=layout_rating_parameters_fkey, referencedTableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
eligibility_set	danielzadorozhniy	classpath:db/changelog/eligibility_set_entity.xml	2020-10-21 11:28:51.468492	55	EXECUTED	8:0caebcc8f10e0d65fe1fe2cf1f4c1c0b	createTable tableName=eligibility_set		\N	3.8.9	\N	\N	3268930328
offer_eligibility_sets	danielzadorozhniy	classpath:db/changelog/product_offering_to_eligibility_set_entity.xml	2020-10-21 11:28:51.48565	56	EXECUTED	8:927de3a76d5c894ad343c6085f3f4e7b	createTable tableName=product_offering_to_eligibility_set		\N	3.8.9	\N	\N	3268930328
metadata	danielzadorozhniy	classpath:db/changelog/eligibility_set_schema_entity.xml	2020-10-21 11:28:51.50661	57	EXECUTED	8:7089455e5a837ffbf205e2113b56bc35	createTable tableName=eligibility_set_schema		\N	3.8.9	\N	\N	3268930328
compatibility_set	danielzadorozhniy	classpath:db/changelog/compatibility_set_entity.xml	2020-10-21 11:28:51.523127	58	EXECUTED	8:4d4e054dcfe055998328ff6383cbca39	createTable tableName=compatibility_set		\N	3.8.9	\N	\N	3268930328
compatibility_set_to_product_offering	danielzadorozhniy	classpath:db/changelog/compatibility_set_to_product_offering_entity.xml	2020-10-21 11:28:51.535782	59	EXECUTED	8:ee4d4ab3df5ef2edf40b465bc7978c60	createTable tableName=compatibility_set_to_product_offering		\N	3.8.9	\N	\N	3268930328
insert_currency_usd	danielzadorozhniy	classpath:db/changelog/currency_lcp_data_for_postman_collection.xml	2020-10-21 11:28:51.547681	60	EXECUTED	8:b0a9f7558566b7d86c64d6da39d38ead	insert tableName=currency		\N	3.8.9	\N	\N	3268930328
insert_lcp_status	danielzadorozhniy	classpath:db/changelog/currency_lcp_data_for_postman_collection.xml	2020-10-21 11:28:51.559486	61	EXECUTED	8:dc564bb5eeeeeaf47fdcc213f4f999e1	insert tableName=lcp_status		\N	3.8.9	\N	\N	3268930328
catalog_calendar_properties-drop-height-column	danielzadorozhniy	classpath:db/changelog/catalog_calendar_properties_new_fields.xml	2020-10-21 11:28:51.573619	62	EXECUTED	8:5c69962517bb3c5b7752e26d034fef15	dropColumn columnName=height, tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
catalog_calendar_properties-make-dom-from-nullable	danielzadorozhniy	classpath:db/changelog/catalog_calendar_properties_new_fields.xml	2020-10-21 11:28:51.586104	63	EXECUTED	8:b6f41f71a683eb63a7a223168f0c436e	dropNotNullConstraint columnName=dom_from, tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
catalog_calendar_properties_new_fields	danielzadorozhniy	classpath:db/changelog/catalog_calendar_properties_new_fields.xml	2020-10-21 11:28:51.628458	64	EXECUTED	8:31c79df194a5d538c2bf67650bb9a7a5	addColumn tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
layout_rating_parameters_drop_unused_columns	danielzadorozhniy	classpath:db/changelog/layout_rating_parameters_update.xml	2020-10-21 11:28:51.648394	65	EXECUTED	8:d1ff29078c12be0c9aad9d6d29f80b66	dropColumn columnName=tariff_id, tableName=layout_rating_parameters; dropColumn columnName=calendar_properties_id, tableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
layout_rating_parameters_to_parameter_update_constraints	danielzadorozhniy	classpath:db/changelog/layout_rating_parameters_update.xml	2020-10-21 11:28:51.666395	66	EXECUTED	8:b54a9d691dbaf97c51236ac5169905db	addForeignKeyConstraint baseTableName=layout_rating_parameters_to_parameter, constraintName=layout_rating_parameters_to_parameter_layout_rating_parameters_fkey, referencedTableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
layout_tags	danielzadorozhniy	classpath:db/changelog/layout_tags.xml	2020-10-21 11:28:51.678655	67	EXECUTED	8:9e8f641fe5fa2b627588abdf188fce45	createTable tableName=layout_tags		\N	3.8.9	\N	\N	3268930328
layout_tags_constraints	danielzadorozhniy	classpath:db/changelog/layout_tags.xml	2020-10-21 11:28:51.691088	68	EXECUTED	8:35db536375707fdca53f87b93edb6758	addForeignKeyConstraint baseTableName=layout_tags, constraintName=layout_tags_layout_rating_parameters_fkey, referencedTableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
subscriber_offer_added_is_active	danielzadorozhniy	classpath:db/changelog/subscriber_offer_add_is_active.xml	2020-10-21 11:28:51.701695	69	EXECUTED	8:1e5a104897f695d71cd0f81d805baa63	addColumn tableName=subscriber_offer		\N	3.8.9	\N	\N	3268930328
subscriber_balance_remove_broken_types	danielzadorozhniy	classpath:db/changelog/subscriber_balance_remove_broken_types.xml	2020-10-21 11:28:51.71177	70	EXECUTED	8:e27463c5cfb0dfc503113a15bf03208a	update tableName=subscriber_balance		\N	3.8.9	\N	\N	3268930328
subscriber_offer_to_subscriber_balance_drop_pks	danielzadorozhniy	classpath:db/changelog/subscriber_offer_to_subscriber_balance_drop_pks.xml	2020-10-21 11:28:51.722828	71	EXECUTED	8:f5d6e289a06b806bb33592be568e2c7b	dropPrimaryKey constraintName=offer_balance_pk, tableName=subscriber_offer_to_subscriber_balance		\N	3.8.9	\N	\N	3268930328
subscriber_offer_activated_at_column	danielzadorozhniy	classpath:db/changelog/subscriber_offer_activated_at_column.xml	2020-10-21 11:28:51.733565	72	EXECUTED	8:651b0c84c75b124c35d939864480cdcc	addColumn tableName=subscriber_offer		\N	3.8.9	\N	\N	3268930328
change_ordinal_to_string_catalog_tariff	alexk	classpath:db/changelog/change_ordinal_to_string.xml	2020-10-21 11:28:51.753348	73	EXECUTED	8:6a8f267274cf65e3e49ba8e1cb41018e	update tableName=catalog_tariff; update tableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
change_ordinal_to_string_catalog_tariff_step	alexk	classpath:db/changelog/change_ordinal_to_string.xml	2020-10-21 11:28:51.774018	74	EXECUTED	8:5c938b837d2fa3539d45e9e1f90f58e7	update tableName=catalog_tariff_step; update tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
change_ordinal_to_string_compatibility_set	alexk	classpath:db/changelog/change_ordinal_to_string.xml	2020-10-21 11:28:51.789477	75	EXECUTED	8:62d68beaba1eaf968656113622eafec1	update tableName=compatibility_set; update tableName=compatibility_set		\N	3.8.9	\N	\N	3268930328
rating_offer_details	alexk	classpath:db/changelog/offer_details.xml	2020-10-21 11:28:51.806247	76	EXECUTED	8:6a150c55de3dd138951e686f881833aa	createTable tableName=rating_offer_details		\N	3.8.9	\N	\N	3268930328
recurring_offer_details	alexk	classpath:db/changelog/offer_details.xml	2020-10-21 11:28:51.822272	77	EXECUTED	8:a106f8f6c96c885d4df535429ec417c9	createTable tableName=recurring_offer_details		\N	3.8.9	\N	\N	3268930328
customer_account_billing_cycle_fields	danielzadorozhniy	classpath:db/changelog/customer_account_billing_cycle_fields.xml	2020-10-21 11:28:51.839992	78	EXECUTED	8:e7260d332d2d59d2a621cb3ec68bcc16	addColumn tableName=customer_account		\N	3.8.9	\N	\N	3268930328
eligibility_schema_added_is_deleted	danielzadorozhniy	classpath:db/changelog/eligibility_schema_added_is_deleted.xml	2020-10-21 11:28:51.850014	79	EXECUTED	8:e6f411fc4ccd6ba1ef44104d30c84579	addColumn tableName=eligibility_set_schema		\N	3.8.9	\N	\N	3268930328
tariff_step_is_adaptive_columns	danielzadorozhniy	classpath:db/changelog/tariff_step_is_adaptive_currency_id_columns.xml	2020-10-21 11:28:51.86574	80	EXECUTED	8:9bb72426d6e02d0bb76ef62e8afdb690	addColumn tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
tariff_step_currency_id_columns	danielzadorozhniy	classpath:db/changelog/tariff_step_is_adaptive_currency_id_columns.xml	2020-10-21 11:28:51.886818	81	EXECUTED	8:cbd2741a83c8df432bd178a7d4edc8d8	addColumn tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
populate-null-values	dkorniichuk	classpath:db/changelog/tariff_step_is_adaptive_currency_id_columns.xml	2020-10-21 11:28:51.900493	82	EXECUTED	8:845f8b7d863a54cc3cd55c7e3ecbc0e0	update tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
add-non-null-constraint	dkorniichuk	classpath:db/changelog/tariff_step_is_adaptive_currency_id_columns.xml	2020-10-21 11:28:51.912412	83	EXECUTED	8:65dd5a1e1c412c3c9a615fe3164dfe41	addNotNullConstraint columnName=currency_id, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
catalog_product_offering_id_primary_and_regular_columns	danielzadorozhniy	classpath:db/changelog/catalog_product_offering_id_primary_and_regular_columns.xml	2020-10-21 11:28:51.926941	84	EXECUTED	8:02b0c8d635055ae209d1ac7555aa86f2	addColumn tableName=catalog_product_offering; addColumn tableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
eligibility_set_schema_is_system_column	danielzadorozhniy	classpath:db/changelog/catalog_product_offering_id_primary_and_regular_columns.xml	2020-10-21 11:28:53.604093	85	EXECUTED	8:3c802b3a441ad566eed112fa93c73236	addColumn tableName=eligibility_set_schema; loadUpdateData tableName=eligibility_set_schema		\N	3.8.9	\N	\N	3268930328
1584944807902-35	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_bundle_to_simple_product_offer_constraint.xml	2020-10-21 11:28:53.621505	86	EXECUTED	8:802431207cf3bb8e8d2cbe1c68de6cbc	addUniqueConstraint constraintName=bundle_to_nested_product_offer_uk01, tableName=catalog_bundle_to_nested_product_offer		\N	3.8.9	\N	\N	3268930328
1584944807902-41	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_bundle_to_simple_product_offer_constraint.xml	2020-10-21 11:28:53.632095	87	EXECUTED	8:d1919765d15eecc3e366991302e037de	addForeignKeyConstraint baseTableName=catalog_bundle_to_nested_product_offer, constraintName=catalog_bundle_to_nested_product_offer_bundle_product_offer_fkey, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
1584944807902-42	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_bundle_to_simple_product_offer_constraint.xml	2020-10-21 11:28:53.64136	88	EXECUTED	8:928d34aa080d0d72536a3f0bc7ec006c	addForeignKeyConstraint baseTableName=catalog_bundle_to_nested_product_offer, constraintName=catalog_nested_to_nested_product_offer_nested_product_offer_fkey, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
1584944807902-36	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_set_to_rating_parameter_constraint.xml	2020-10-21 11:28:53.651981	89	EXECUTED	8:92caccc22b4475a5443aa4587115203d	addUniqueConstraint constraintName=catalog_rating_parameter_set_to_rating_parameter_uk01, tableName=catalog_rating_parameter_set_to_rating_parameter		\N	3.8.9	\N	\N	3268930328
catalog_rating_parameter_set_to_rating_parameter_rating_parameter_set_fkey	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_set_to_rating_parameter_constraint.xml	2020-10-21 11:28:53.661678	90	EXECUTED	8:1377c0aa333cb206d15039e03b3fded3	addForeignKeyConstraint baseTableName=catalog_rating_parameter_set_to_rating_parameter, constraintName=rating_parameter_set_fkey, referencedTableName=catalog_rating_parameter_set		\N	3.8.9	\N	\N	3268930328
catalog_rating_parameter_set_to_rating_parameter_rating_parameter_fkey	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_set_to_rating_parameter_constraint.xml	2020-10-21 11:28:53.675498	91	EXECUTED	8:065aa213ce67b0afe13de371e2eec1f9	addForeignKeyConstraint baseTableName=catalog_rating_parameter_set_to_rating_parameter, constraintName=rating_parameter_fkey, referencedTableName=catalog_rating_parameter		\N	3.8.9	\N	\N	3268930328
1584944807902-37	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_calendar_properties_constraint.xml	2020-10-21 11:28:53.690871	92	EXECUTED	8:1d657ce0322cb3dbdf3c852a00a55edc	addUniqueConstraint constraintName=catalog_calendar_properties_uk01, tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
1584944807902-43	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_calendar_properties_constraint.xml	2020-10-21 11:28:53.706719	93	EXECUTED	8:26b4e870d97c5a6143bb02f3219207ff	addForeignKeyConstraint baseTableName=catalog_calendar_properties, constraintName=catalog_calendar_properties_calendar_fkey, referencedTableName=catalog_calendar		\N	3.8.9	\N	\N	3268930328
drop_unique_constraint_from_catalog_calendar_properties	alexkosholap	classpath:db/changelog/202003231650_catalog_calendar_properties_constraint.xml	2020-10-21 11:28:53.717973	94	EXECUTED	8:92f1027a03a161d6fcebdc7d230c4776	dropUniqueConstraint constraintName=catalog_calendar_properties_uk01, tableName=catalog_calendar_properties		\N	3.8.9	\N	\N	3268930328
1584944807902-38	danielzadorozhniy	classpath:db/changelog/202003231650_exchange_rate_constraint.xml	2020-10-21 11:28:53.738595	95	EXECUTED	8:61a0ea9f107ad0ac850abc9a6a7741f2	addUniqueConstraint constraintName=exchange_rate_uk01, tableName=exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807902-60	danielzadorozhniy	classpath:db/changelog/202003231650_exchange_rate_constraint.xml	2020-10-21 11:28:53.75832	96	EXECUTED	8:29373450e53445be670c48e4da7c3db2	addForeignKeyConstraint baseTableName=exchange_rate, constraintName=exchange_rate_currency_fkey, referencedTableName=currency		\N	3.8.9	\N	\N	3268930328
1584944807902-67	danielzadorozhniy	classpath:db/changelog/202003231650_exchange_rate_constraint.xml	2020-10-21 11:28:53.774417	97	EXECUTED	8:200604b1d8a80395228619bce3fecbf8	addForeignKeyConstraint baseTableName=exchange_rate, constraintName=exchange_rate_provider_fkey, referencedTableName=catalog_provider		\N	3.8.9	\N	\N	3268930328
1584944807902-40	danielzadorozhniy	classpath:db/changelog/202003231650_unit_exchange_rate_constraint.xml	2020-10-21 11:28:53.787872	98	EXECUTED	8:c95c1cc0f32541c3ebbdd7b86342cf2b	addUniqueConstraint constraintName=unit_exchange_rate_uk01, tableName=unit_exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807902-44	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_layout_constraint.xml	2020-10-21 11:28:53.800203	99	EXECUTED	8:5aed0a6b72f368b954fc5f00129c315e	addForeignKeyConstraint baseTableName=catalog_layout, constraintName=catalog_layout_rating_parameter_set_fkey, referencedTableName=catalog_rating_parameter_set		\N	3.8.9	\N	\N	3268930328
1584944807902-45	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_offer_price_constraint.xml	2020-10-21 11:28:53.812597	100	EXECUTED	8:b2e8198f6d35dae0895ad9beb10b3bfa	addForeignKeyConstraint baseTableName=catalog_offer_price, constraintName=catalog_offer_price_currency_fkey, referencedTableName=currency		\N	3.8.9	\N	\N	3268930328
1584944807902-46	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_offer_term_constraint.xml	2020-10-21 11:28:53.823025	101	EXECUTED	8:88a495e6e76f156a0f07969622827a9a	addForeignKeyConstraint baseTableName=catalog_offer_term, constraintName=catalog_offer_term_recurring_type_fkey, referencedTableName=catalog_recurring_type		\N	3.8.9	\N	\N	3268930328
1584944807902-65	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_offer_term_constraint.xml	2020-10-21 11:28:53.835415	102	EXECUTED	8:d0d9b2b651cfce284de09f110d134333	addForeignKeyConstraint baseTableName=catalog_offer_term, constraintName=catalog_offer_term_tariff_plan_fkey, referencedTableName=catalog_tariff_plan		\N	3.8.9	\N	\N	3268930328
1584944807902-47	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_price_item_constraint.xml	2020-10-21 11:28:53.847115	103	EXECUTED	8:3e73fad691b5eb824c94221cd1e867e6	addForeignKeyConstraint baseTableName=catalog_price_item, constraintName=catalog_price_item_currency_fkey, referencedTableName=currency		\N	3.8.9	\N	\N	3268930328
catalog_product_offering_usage_balance_id_fkey	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_constraint.xml	2020-10-21 11:28:53.856766	104	EXECUTED	8:b855113e9246e2782a46474a614b6fea	addForeignKeyConstraint baseTableName=catalog_product_offering, constraintName=catalog_product_offering_usage_balance_id_fkey, referencedTableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
catalog_product_offering_periodic_balance_id_fkey	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_constraint.xml	2020-10-21 11:28:53.87395	105	EXECUTED	8:7b542c0111b40d368f239d9fbf3624c0	addForeignKeyConstraint baseTableName=catalog_product_offering, constraintName=catalog_product_offering_periodic_balance_id_fkey, referencedTableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-49	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_constraint.xml	2020-10-21 11:28:53.892297	106	EXECUTED	8:e8c88c4e584277da3a111286de5cb2f9	addForeignKeyConstraint baseTableName=catalog_product_offering, constraintName=catalog_product_offering_offer_term_fkey, referencedTableName=catalog_offer_term		\N	3.8.9	\N	\N	3268930328
1584944807902-50	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_constraint.xml	2020-10-21 11:28:53.911352	107	EXECUTED	8:dac476f6f49b1115e5f4227b69abee18	addForeignKeyConstraint baseTableName=catalog_product_offering, constraintName=catalog_product_offering_price_fkey, referencedTableName=catalog_offer_price		\N	3.8.9	\N	\N	3268930328
1584944807902-69	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_offering_constraint.xml	2020-10-21 11:28:53.92772	108	EXECUTED	8:2a9ece21e40db8e003d28f50443d5c80	addForeignKeyConstraint baseTableName=catalog_product_offering, constraintName=catalog_product_offering_product_fkey, referencedTableName=catalog_product		\N	3.8.9	\N	\N	3268930328
1584944807902-51	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_product_constraint.xml	2020-10-21 11:28:53.939629	109	EXECUTED	8:f4afffa43c4cb6875ba1fa69bf925e1f	addForeignKeyConstraint baseTableName=catalog_product, constraintName=catalog_product_service_fkey, referencedTableName=catalog_service		\N	3.8.9	\N	\N	3268930328
1584944807902-52	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_plan_constraint.xml	2020-10-21 11:28:53.950244	110	EXECUTED	8:dbf23d0c13f94cae29012f89749306b5	addForeignKeyConstraint baseTableName=catalog_tariff_plan, constraintName=catalog_tariff_plan_calendar_fkey, referencedTableName=catalog_calendar		\N	3.8.9	\N	\N	3268930328
1584944807902-53	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_plan_constraint.xml	2020-10-21 11:28:53.96094	111	EXECUTED	8:30bd0d0cf3de6d5d79bba1d8741a3263	addForeignKeyConstraint baseTableName=catalog_tariff_plan, constraintName=catalog_tariff_plan_layout_fkey, referencedTableName=catalog_layout		\N	3.8.9	\N	\N	3268930328
1584944807902-54	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_step_constraint.xml	2020-10-21 11:28:53.972292	112	EXECUTED	8:213ef046df8d155518c1ce00592467a1	addForeignKeyConstraint baseTableName=catalog_tariff_step, constraintName=catalog_tariff_step_price_item_fkey, referencedTableName=catalog_price_item		\N	3.8.9	\N	\N	3268930328
1584944807902-55	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_tariff_step_constraint.xml	2020-10-21 11:28:53.983042	113	EXECUTED	8:c1b977a05ce771d95929ca200f56acc6	addForeignKeyConstraint baseTableName=catalog_tariff_step, constraintName=catalog_tariff_step_tariff_fkey, referencedTableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
1584944807902-56	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_version_properties_constraint.xml	2020-10-21 11:28:53.993621	114	EXECUTED	8:0ae322c1909298da6c657a7f313a605b	addForeignKeyConstraint baseTableName=catalog_version_properties, constraintName=catalog_version_properties_version_fkey, referencedTableName=catalog_version		\N	3.8.9	\N	\N	3268930328
1584944807902-57	danielzadorozhniy	classpath:db/changelog/202003231650_customer_account_constraint.xml	2020-10-21 11:28:54.003797	115	EXECUTED	8:00ebcc44928a3966669ccfe56c8e6461	addForeignKeyConstraint baseTableName=customer_account, constraintName=customer_account_currency_fkey, referencedTableName=currency		\N	3.8.9	\N	\N	3268930328
1584944807902-58	danielzadorozhniy	classpath:db/changelog/202003231650_customer_account_constraint.xml	2020-10-21 11:28:54.014287	116	EXECUTED	8:5d109e8a5b6759c025339c7adc3fa62f	addForeignKeyConstraint baseTableName=customer_account, constraintName=customer_account_parent_fkey, referencedTableName=customer_account		\N	3.8.9	\N	\N	3268930328
1584944807902-64	danielzadorozhniy	classpath:db/changelog/202003231650_customer_account_constraint.xml	2020-10-21 11:28:54.024205	117	EXECUTED	8:d8a43f44eef09024f84a792445f845ed	addForeignKeyConstraint baseTableName=customer_account, constraintName=customer_account_customer_fkey, referencedTableName=customer		\N	3.8.9	\N	\N	3268930328
1584944807902-59	danielzadorozhniy	classpath:db/changelog/202003231650_customer_constraint.xml	2020-10-21 11:28:54.033686	118	EXECUTED	8:3d17780dca0a008b6cc49fae2a611570	addForeignKeyConstraint baseTableName=customer, constraintName=customer_parent_fkey, referencedTableName=customer		\N	3.8.9	\N	\N	3268930328
1584944807902-61	danielzadorozhniy	classpath:db/changelog/202003231650_lcp_status_service_constraint.xml	2020-10-21 11:28:54.042481	119	EXECUTED	8:bd5d5e6087e1051e7940ddba0c2456b7	addForeignKeyConstraint baseTableName=lcp_status_service, constraintName=lcp_status_service_status_fkey, referencedTableName=lcp_status		\N	3.8.9	\N	\N	3268930328
1584944807902-62	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_balance_constraint.xml	2020-10-21 11:28:54.051787	120	EXECUTED	8:8b0137bd51fcd5619e436ea870b5ff0d	addForeignKeyConstraint baseTableName=subscriber_balance, constraintName=subscriber_balance_parent_fkey, referencedTableName=subscriber_balance		\N	3.8.9	\N	\N	3268930328
1584944807902-82	dkorniichuk	classpath:db/changelog/202003231650_subscriber_balance_constraint.xml	2020-10-21 11:28:54.060811	121	EXECUTED	8:2a8520528de937715093aeab18140f41	addForeignKeyConstraint baseTableName=subscriber_balance, constraintName=subscriber_balance_subscriber_fkey, referencedTableName=subscriber		\N	3.8.9	\N	\N	3268930328
1584944807902-63	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_constraint.xml	2020-10-21 11:28:54.070321	122	EXECUTED	8:0a913d55a4086dc58d840c5dd242b6db	addForeignKeyConstraint baseTableName=subscriber, constraintName=subscriber_customer_account_fkey, referencedTableName=customer_account		\N	3.8.9	\N	\N	3268930328
1584944807902-68	danielzadorozhniy	classpath:db/changelog/202003231650_subscriber_offer_constraint.xml	2020-10-21 11:28:54.080388	123	EXECUTED	8:88f82f47428a64f7492bd292d17979ad	addForeignKeyConstraint baseTableName=subscriber_offer, constraintName=subscriber_offer_subscriber_fkey, referencedTableName=subscriber		\N	3.8.9	\N	\N	3268930328
1584944807902-77	dkorniichuk	classpath:db/changelog/202003231650_subscriber_offer_constraint.xml	2020-10-21 11:28:54.090824	124	EXECUTED	8:62a84bcaa84d130ecabc16c6dabf3525	addForeignKeyConstraint baseTableName=subscriber_offer, constraintName=subscriber_offer_product_offering_fkey, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
1584944807902-66	danielzadorozhniy	classpath:db/changelog/202003231650_catalog_rating_parameter_constraint.xml	2020-10-21 11:28:54.105396	125	EXECUTED	8:05a35ee0c796ef9966c432cca8f21970	addForeignKeyConstraint baseTableName=catalog_rating_parameter, constraintName=catalog_rating_parameter_custom_list_fkey, referencedTableName=catalog_custom_list		\N	3.8.9	\N	\N	3268930328
1589959696926-14	danielzadorozhniy	classpath:db/changelog/product_offering_to_eligibility_set_constraint.xml	2020-10-21 11:28:54.118144	126	EXECUTED	8:03be4ba5273987654a0f74818837b2d0	addForeignKeyConstraint baseTableName=product_offering_to_eligibility_set, constraintName=product_offering_to_eligibility_set_catalog_product_offering_id_fk, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
1589959696926-15	danielzadorozhniy	classpath:db/changelog/product_offering_to_eligibility_set_constraint.xml	2020-10-21 11:28:54.136126	127	EXECUTED	8:ee8244c860ebb3de2d82b10b980963b4	addForeignKeyConstraint baseTableName=product_offering_to_eligibility_set, constraintName=product_offering_to_eligibility_set_eligibility_set_id_fk, referencedTableName=eligibility_set		\N	3.8.9	\N	\N	3268930328
compatibility_set_to_product_offering_catalog_product_offering_id_fk	danielzadorozhniy	classpath:db/changelog/compatibility_set_to_product_offering_constraint.xml	2020-10-21 11:28:54.15041	128	EXECUTED	8:0215181f109ff33f7f6ae0966cb4317d	addForeignKeyConstraint baseTableName=compatibility_set_to_product_offering, constraintName=compatibility_set_to_product_offering_catalog_product_offering_id_fk, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
compatibility_set_to_product_offering_compatibility_set_id_fk	danielzadorozhniy	classpath:db/changelog/compatibility_set_to_product_offering_constraint.xml	2020-10-21 11:28:54.164454	129	EXECUTED	8:340fa21cb50801d16065fce03f8c4d9f	addForeignKeyConstraint baseTableName=compatibility_set_to_product_offering, constraintName=compatibility_set_to_product_offering_compatibility_set_id_fk, referencedTableName=compatibility_set		\N	3.8.9	\N	\N	3268930328
compatibility_set_to_product_offering_compatibility_set_id_product_offering_id_uindex	danielzadorozhniy	classpath:db/changelog/compatibility_set_to_product_offering_constraint.xml	2020-10-21 11:28:54.17928	130	EXECUTED	8:5588c9afadd88ebf0b3b75792287d825	createIndex indexName=compatibility_set_to_product_offering_compatibility_set_id_product_offering_id_uindex, tableName=compatibility_set_to_product_offering		\N	3.8.9	\N	\N	3268930328
1584944807992-34	dkorniichuk	classpath:db/changelog/202006161630_exchange_rates_structure_changes.xml	2020-10-21 11:28:54.189446	131	EXECUTED	8:935def7c84818d353e838237ad575621	dropTable tableName=unit_exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807992-35	dkorniichuk	classpath:db/changelog/202006161630_exchange_rates_structure_changes.xml	2020-10-21 11:28:54.199357	132	EXECUTED	8:6322284281601edd1c88f82b09b48400	addColumn tableName=currency		\N	3.8.9	\N	\N	3268930328
1584944807992-35	dmytrokorniichuk	classpath:db/changelog/202006161630_exchange_rates_structure_changes.xml	2020-10-21 11:28:54.208373	133	EXECUTED	8:961b5a719678b5ae95979e121a6c005f	addColumn tableName=exchange_rate		\N	3.8.9	\N	\N	3268930328
1584944807903-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.218283	134	EXECUTED	8:1401a313544359f5dbdfe5f2bf431432	addColumn tableName=subscriber_balance		\N	3.8.9	\N	\N	3268930328
1584948887943-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.228137	135	EXECUTED	8:392a22014fc3820da1d2258d445dbd48	update tableName=subscriber_balance		\N	3.8.9	!production	\N	3268930328
1584948887943-19	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.240793	136	EXECUTED	8:dc4380b713ea6a5130a26bd2ddd570e6	addNotNullConstraint columnName=realm, tableName=subscriber_balance		\N	3.8.9	\N	\N	3268930328
1584948887903-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.251604	137	EXECUTED	8:47d6a2a1ff4297976032a8ddc1efaa00	update tableName=subscriber_balance		\N	3.8.9	!production	\N	3268930328
1584948887904-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.265158	138	EXECUTED	8:08556caf2f26ee58772b1c47af6e5148	update tableName=subscriber_balance		\N	3.8.9	!production	\N	3268930328
1584948887905-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.277111	139	EXECUTED	8:4f588498a31cd632db447d367b926c12	update tableName=catalog_balance		\N	3.8.9	!production	\N	3268930328
1584948887906-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.28726	140	EXECUTED	8:95bf81d38b92ba9875ecd6cb5b0fe4bd	update tableName=catalog_balance		\N	3.8.9	!production	\N	3268930328
1584944857903-69	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.299791	141	EXECUTED	8:78c51cf3d5f2889f051e0b1883eaa458	renameColumn newColumnName=currency, oldColumnName=unit_type, tableName=subscriber_balance; renameColumn newColumnName=currency, oldColumnName=unit_type, tableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
1584944807982-77	dkorniichuk	classpath:db/changelog/202006231100_subscriber_balance_changes.xml	2020-10-21 11:28:54.317545	142	EXECUTED	8:cdd949384c79664760fd68f657b693b7	addForeignKeyConstraint baseTableName=subscriber_balance, constraintName=subscriber_balance_currency_fkey, referencedTableName=currency; addForeignKeyConstraint baseTableName=catalog_balance, constraintName=catalog_balance_currency_fkey, reference...		\N	3.8.9	\N	\N	3268930328
subscriber_path	dkorniichuk	classpath:db/changelog/subscriber_add_path_column.xml	2020-10-21 11:28:54.329996	143	EXECUTED	8:369b0637696ddbdb7bd2f7e11df9077a	addColumn tableName=subscriber		\N	3.8.9	\N	\N	3268930328
product_offering_to_compatibility_set	dkorniichuk	classpath:db/changelog/product_offering_to_compatibility_set_entity.xml	2020-10-21 11:28:54.3401	144	EXECUTED	8:d4c09e286e714a16efa49e1f1ae2dcc0	createTable tableName=product_offering_to_compatibility_set		\N	3.8.9	\N	\N	3268930328
catalog_product_offering_product_offering_to_compatibility_set_id_fk	dkorniichuk	classpath:db/changelog/product_offering_to_compatibility_set_constraint.xml	2020-10-21 11:28:54.350629	145	EXECUTED	8:cba0db04afaee8e77ca63f3cc02a7f6c	addForeignKeyConstraint baseTableName=product_offering_to_compatibility_set, constraintName=catalog_product_offering_product_offering_to_compatibility_set_id_fk, referencedTableName=catalog_product_offering		\N	3.8.9	\N	\N	3268930328
product_offering_to_compatibility_set_compatibility_set_id_fk	dkorniichuk	classpath:db/changelog/product_offering_to_compatibility_set_constraint.xml	2020-10-21 11:28:54.36136	146	EXECUTED	8:c87080ba995ef10f1411a4441d1fed51	addForeignKeyConstraint baseTableName=product_offering_to_compatibility_set, constraintName=product_offering_to_compatibility_set_compatibility_set_id_fk, referencedTableName=compatibility_set		\N	3.8.9	\N	\N	3268930328
eligibility_set_expression	dkorniichuk	classpath:db/changelog/eligibility_set_add_expression_column.xml	2020-10-21 11:28:54.371292	147	EXECUTED	8:a6a4a68dee569cc281e319b585963fe4	addColumn tableName=eligibility_set		\N	3.8.9	\N	\N	3268930328
1584944327902-69	dkorniichuk	classpath:db/changelog/add_name_column_to_balance.xml	2020-10-21 11:28:54.380808	148	EXECUTED	8:a9b70dc97c50879a970195afd43fcb5a	addColumn tableName=catalog_balance		\N	3.8.9	\N	\N	3268930328
recurring_offer_details_new_columns	danielzadorozhniy	classpath:db/changelog/recurring_offer_details_new_columns.xml	2020-10-21 11:28:54.398414	149	EXECUTED	8:ef904bf6dd522510d7c1637c1bf4b229	addColumn tableName=recurring_offer_details		\N	3.8.9	\N	\N	3268930328
recurring_offer_details_subscriber_fk	danielzadorozhniy	classpath:db/changelog/recurring_offer_details_new_columns.xml	2020-10-21 11:28:54.410473	150	EXECUTED	8:f96e447daa5ad265b48ea300dcd86dc1	addForeignKeyConstraint baseTableName=recurring_offer_details, constraintName=subscriber_id_recurring_offer_details_fk, referencedTableName=subscriber		\N	3.8.9	\N	\N	3268930328
rating_offer_details_new_columns	dkorniichuk	classpath:db/changelog/rating_offer_details_new_columns.xml	2020-10-21 11:28:54.423588	151	EXECUTED	8:fd93347a3a02fa223e81899f3e82d7bc	addColumn tableName=rating_offer_details		\N	3.8.9	\N	\N	3268930328
rating_offer_details_subscriber_fk	dkorniichuk	classpath:db/changelog/rating_offer_details_new_columns.xml	2020-10-21 11:28:54.434981	152	EXECUTED	8:e2e87a942c62a5ee52303f2ef611d7f8	addForeignKeyConstraint baseTableName=rating_offer_details, constraintName=subscriber_id_rating_offer_details_fk, referencedTableName=subscriber		\N	3.8.9	\N	\N	3268930328
groups	dkorniichuk	classpath:db/changelog/layout_rating_parameters_group_entity.xml	2020-10-21 11:28:54.45145	153	EXECUTED	8:00b983a030a9c696015405dd9f799fd7	createTable tableName=layout_rating_parameters_group		\N	3.8.9	\N	\N	3268930328
groups-constraint-new-columns	dkorniichuk	classpath:db/changelog/layout_rating_parameters_add_group_column.xml	2020-10-21 11:28:54.465536	154	EXECUTED	8:ccb57c41a53d4a912d5341ce05448b1d	addColumn tableName=layout_rating_parameters; addColumn tableName=layout_rating_parameters		\N	3.8.9	\N	\N	3268930328
groups-constraint	dkorniichuk	classpath:db/changelog/layout_rating_parameters_add_group_column.xml	2020-10-21 11:28:54.478044	155	EXECUTED	8:37fa738f353f32d749dd1e84033d8f03	addForeignKeyConstraint baseTableName=layout_rating_parameters, constraintName=layout_rating_parameters_layout_rating_parameters_group_fkey, referencedTableName=layout_rating_parameters_group		\N	3.8.9	\N	\N	3268930328
currency_type	dkorniichuk	classpath:db/changelog/currency_type_column.xml	2020-10-21 11:28:54.494151	156	EXECUTED	8:794035af6b5e2c0c662b88805ab856bb	addColumn tableName=currency; update tableName=currency; update tableName=currency		\N	3.8.9	\N	\N	3268930328
currency_monetary_delete	dkorniichuk	classpath:db/changelog/currency_type_column.xml	2020-10-21 11:28:54.506345	157	EXECUTED	8:65095147763a258390daf572a0584fe1	dropColumn columnName=monetary, tableName=currency		\N	3.8.9	\N	\N	3268930328
currency_monetary_delete	dkorniichuk	classpath:db/changelog/drop_tariff_step_type_column.xml	2020-10-21 11:28:54.52666	158	EXECUTED	8:5e766f27e643f4fc23797fde7f3b8b18	dropColumn columnName=type, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
remove-non-null-constraint	dkorniichuk	classpath:db/changelog/drop_currency_id_non_null_constraint_tariff_step.xml	2020-10-21 11:28:54.541798	159	EXECUTED	8:14671a8d909f2ec039c8533249a2193b	dropNotNullConstraint columnName=currency_id, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
add_rating_offer_details_subscriber_index	alexk	initial/tables.xml	2020-10-21 11:28:54.921326	160	EXECUTED	8:e7392263c568e2f675d99d0d81719973	createIndex indexName=subscriber_id_idx, tableName=rating_offer_details		\N	3.8.9	\N	\N	3268930328
created_date_updated_date_columns	danielzadorozhniy	classpath:db/changelog/created_date_updated_date_columns.xml	2020-10-21 11:28:55.078158	161	EXECUTED	8:96ebc73b4dc01e1744990e330a1434c7	addColumn tableName=catalog_balance; addColumn tableName=catalog_balance; addColumn tableName=subscriber; addColumn tableName=subscriber; addColumn tableName=subscriber_offer; addColumn tableName=subscriber_offer; addColumn tableName=subscriber_ba...		\N	3.8.9	\N	\N	3268930328
rating_offer_details_is_primary	dkorniichuk	classpath:db/changelog/is_primary_to_rating_offer_details.xml	2020-10-21 11:28:55.089413	162	EXECUTED	8:80754ddd8836e362d7e383f2a487a1da	addColumn tableName=rating_offer_details		\N	3.8.9	\N	\N	3268930328
populate_updated_date	alexk	classpath:db/changelog/populate_updated_date.xml	2020-10-21 11:28:55.169779	163	EXECUTED	8:230d77a2ad00e8d1173c2b81af3c7d24	sql		\N	3.8.9	\N	\N	3268930328
tariff_is_adaptive	dkorniichuk	classpath:db/changelog/move_is_adaptive_to_tariff_level.xml	2020-10-21 11:28:55.185848	164	EXECUTED	8:3ca15a6f7e995d6844b8ecffa3d900d6	addColumn tableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
drop_is_adaptive_from_tariff_step	dkorniichuk	classpath:db/changelog/move_is_adaptive_to_tariff_level.xml	2020-10-21 11:28:55.208059	165	EXECUTED	8:97321a719189ee303ebec063537e4141	dropColumn columnName=is_adaptive, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
drop_non_nullable_value_constraint	dkorniichuk	classpath:db/changelog/remove_tariff_step_value_not_null_constraint.xml	2020-10-21 11:28:55.218452	166	EXECUTED	8:7fbc4dcab084b886cdca2b86ac01367a	dropNotNullConstraint columnName=value, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
drop_non_nullable_repetitions_constraint	alexk	classpath:db/changelog/remove_tariff_step_repetitions_not_null_constraint.xml	2020-10-21 11:28:55.229557	167	EXECUTED	8:e3408821cdb162635b0002b98da9dc93	dropNotNullConstraint columnName=repetitions, tableName=catalog_tariff_step		\N	3.8.9	\N	\N	3268930328
add_lookup_key_index	alexp	initial/tables.xml	2020-10-21 11:28:55.620007	168	EXECUTED	8:6817337a0639e7ff64f21d31f1d25434	createIndex indexName=lookup_key_idx, tableName=lookup		\N	3.8.9	\N	\N	3268930328
load_system_tariffs	danielzadorozhniy	classpath:db/changelog/system_tariffs.xml	2020-10-21 11:28:58.336408	169	EXECUTED	8:a73114ab3e2d9c489787077ef30404c7	loadUpdateData tableName=catalog_tariff		\N	3.8.9	\N	\N	3268930328
remove_legacy_data	dkorniichuk	classpath:db/changelog/exchange_rate_data.xml	2020-10-21 11:28:58.344039	170	MARK_RAN	8:342cdb629e44f59f45b20a061357dc9b	delete tableName=exchange_rate		\N	3.8.9	!production	\N	3268930328
load_duration_currency	dkorniichuk	classpath:db/changelog/currency_duration_type_data.xml	2020-10-21 11:29:01.06584	171	EXECUTED	8:834bff3a2111e6d30d62f5ee27e73e92	loadUpdateData tableName=currency		\N	3.8.9	\N	\N	3268930328
load_data	alexk	classpath:db/changelog/data.xml	2020-10-21 11:29:03.864095	172	EXECUTED	8:38c66874f0d04383f6959d0de8298d7f	loadUpdateData tableName=currency		\N	3.8.9	\N	\N	3268930328
load_exchange_rates	dkorniichuk	classpath:db/changelog/exchange_rate_data.xml	2020-10-21 11:29:06.683244	173	EXECUTED	8:567673d49d70e70bddf40cf90ac65075	loadUpdateData tableName=exchange_rate		\N	3.8.9	!production	\N	3268930328
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: eligibility_set; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.eligibility_set (id, name, description, condition, schema_id, is_deleted, expression, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: eligibility_set_schema; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.eligibility_set_schema (id, schema, is_deleted, is_system, created_date, updated_date) FROM stdin;
aab12ccd-87e3-4879-83a1-eb563787cf1e	{}	f	t	2020-10-21 11:28:54.928063	2020-10-21 11:28:54.928063
\.


--
-- Data for Name: exchange_rate; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.exchange_rate (id, realm, provider, currency, rate, effective_date_from, effective_date_to, is_deleted, created_date, updated_date) FROM stdin;
c1eb2769-d469-4896-aeab-1f35817881b9		\N	USD	9	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
faa45940-b697-4d2d-90cc-c5ab6f3a01f0		\N	EUR	11	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
4372bb3d-8acf-4433-a2a8-d65f293f3f8b		\N	GB 	3	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
5bf7a5f4-37b2-403e-b468-ec6c783227b9		\N	MB 	2	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
8a1e1365-a077-473a-8280-75c39c7afa81		\N	KB 	1	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
8633f403-f49a-4a75-956e-4b140932eec3		\N	SMS	2	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
0484b40a-115a-4ed6-b42e-df03a7c5c68a		\N	MIN	6	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
27fd8f93-7450-4242-beac-e0fc93be6a84		\N	SEC	1	2010-01-01 00:00:00	\N	f	2020-10-21 11:29:03.869749	\N
\.


--
-- Data for Name: layout_rating_parameters; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.layout_rating_parameters (id, "position", group_id, is_deleted, created_date, updated_date) FROM stdin;
f27802b4-c210-4204-90db-31df6146e124	0	\N	f	2020-10-21 11:29:37.684	\N
7fcb7d67-2616-4c7b-9e83-a80269c5551e	0	\N	f	2020-10-21 11:29:38.218	\N
d5316ae4-4ae3-4c20-ad85-b8fa8a4c695b	0	\N	f	2020-10-21 11:29:38.482	\N
f9bfb28c-fe2c-4ed7-9719-2fb75791a88a	0	\N	f	2020-10-21 11:29:38.788	\N
e3e7a903-8775-4d59-9ebd-3ea80e762aa8	0	\N	f	2020-10-21 11:29:39.056	\N
b98828f0-3417-47cd-b829-084847f61aae	0	\N	f	2020-10-21 11:29:39.344	\N
\.


--
-- Data for Name: layout_rating_parameters_group; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.layout_rating_parameters_group (id, name, parameters, is_deleted, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: layout_rating_parameters_to_parameter; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.layout_rating_parameters_to_parameter (layout_rating_parameters, parameter) FROM stdin;
f27802b4-c210-4204-90db-31df6146e124	+38063
f27802b4-c210-4204-90db-31df6146e124	+38044
7fcb7d67-2616-4c7b-9e83-a80269c5551e	+38063
7fcb7d67-2616-4c7b-9e83-a80269c5551e	+38044
d5316ae4-4ae3-4c20-ad85-b8fa8a4c695b	+38063
d5316ae4-4ae3-4c20-ad85-b8fa8a4c695b	+38044
f9bfb28c-fe2c-4ed7-9719-2fb75791a88a	+38063
f9bfb28c-fe2c-4ed7-9719-2fb75791a88a	+38044
e3e7a903-8775-4d59-9ebd-3ea80e762aa8	+38063
e3e7a903-8775-4d59-9ebd-3ea80e762aa8	+38044
b98828f0-3417-47cd-b829-084847f61aae	+38063
b98828f0-3417-47cd-b829-084847f61aae	+38044
\.


--
-- Data for Name: layout_tags; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.layout_tags (layout_rating_parameters, tag) FROM stdin;
f27802b4-c210-4204-90db-31df6146e124	TaXDg
7fcb7d67-2616-4c7b-9e83-a80269c5551e	wPIdF
d5316ae4-4ae3-4c20-ad85-b8fa8a4c695b	SliUR
f9bfb28c-fe2c-4ed7-9719-2fb75791a88a	MfhNn
e3e7a903-8775-4d59-9ebd-3ea80e762aa8	QDjeD
b98828f0-3417-47cd-b829-084847f61aae	mTPkd
\.


--
-- Data for Name: lcp_status; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.lcp_status (status) FROM stdin;
status
\.


--
-- Data for Name: lcp_status_service; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.lcp_status_service (status, service) FROM stdin;
\.


--
-- Data for Name: lookup; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.lookup (id, module, key, realm, value, effective_date_from, effective_date_to, is_deleted) FROM stdin;
9cd3d899-3ab0-4138-a1e5-3a3833ab908b	subscriber	b8434726-4f7f-49c9-b825-158f42a3b022		b8434726-4f7f-49c9-b825-158f42a3b022	2020-10-21 11:29:37.242	\N	f
\.


--
-- Data for Name: product_offering_to_compatibility_set; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.product_offering_to_compatibility_set (compatibility_set_id, product_offering_id) FROM stdin;
\.


--
-- Data for Name: product_offering_to_eligibility_set; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.product_offering_to_eligibility_set (eligibility_set_id, product_offering_id) FROM stdin;
\.


--
-- Data for Name: rating_offer_details; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.rating_offer_details (id, subscriber_offer_id, product_offering_id, service_id, product_id, tariff_plan_id, usage_balance_id, periodic_balance_id, effective_date_from, effective_date_to, priority, subscriber_id, is_primary) FROM stdin;
\.


--
-- Data for Name: recurring_offer_details; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.recurring_offer_details (id, subscriber_offer_id, product_offering_id, service_id, product_id, price_item_currency, price_item_value, effective_date_from, effective_date_to, usage_balance_id, periodic_balance_id, allowance, recurring_type, recurring_value, is_recurring_prorated, is_recurring_calendar, next_charge_date, last_charge_date, is_primary, priority, subscriber_id) FROM stdin;
\.


--
-- Data for Name: subscriber; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.subscriber (id, version, customer_account, is_deleted, is_virtual, status, path, created_date, updated_date) FROM stdin;
b8434726-4f7f-49c9-b825-158f42a3b022	\N	402bf54d-dbba-4d3c-a826-9b6aec347c2c	f	t	\N	\N	2020-10-21 11:29:36.812	2020-10-21 11:29:36.812
\.


--
-- Data for Name: subscriber_balance; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.subscriber_balance (id, parent, type, currency, balance, is_virtual, minimum_balance, maximum_balance, subscriber, subscriber_offer, is_deleted, realm, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: subscriber_offer; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.subscriber_offer (id, description, is_primary, is_deleted, subscriber, product_offering, offer_details, realm, name, priority, is_active, activated_at, created_date, updated_date) FROM stdin;
\.


--
-- Data for Name: subscriber_offer_to_subscriber_balance; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.subscriber_offer_to_subscriber_balance (subscriber_offer, subscriber_balance) FROM stdin;
\.


--
-- Data for Name: tariff_calendar_range; Type: TABLE DATA; Schema: public; Owner: mcadmin
--

COPY public.tariff_calendar_range (id, "position", tariff_id, calendar_properties_id) FROM stdin;
6edbe659-1e68-4510-befa-eaada22bd45f	0	ecbbd7bd-da5f-4aa2-a079-0addac827973	622e7537-5a68-4be8-97ce-84c5fc551d01
a0e202e9-f68e-4f4c-b8ad-14eab13714e9	0	3d4de345-63ee-42a7-994c-dc4e2adf2744	8c3ba17d-f882-4c76-ad79-a83e48069c36
bb10e581-54c7-4cc2-899b-8ff2ebb408d3	0	6ecee306-e81a-46a2-9088-d755c3dce962	108c0f3d-6714-4885-9190-2c9ce42a6189
c5a60fc5-669b-4ecc-80c9-c36943ecccdf	0	b6f42738-b2bc-4ac9-95cb-7da5624113d6	6ee65672-4590-4e02-a0b4-3012ad211ce4
43f9e252-52fa-4595-8a46-0101c0fe43a5	0	6af3ef92-9c6c-483f-a894-b3ce559bb434	c409237e-f3c8-4299-ab42-1ad2fe36bcac
db587c71-b199-4830-8836-9f387a7bb72c	0	6c0dd2e5-fb84-42a0-a691-10c3b1f7e2c0	ea13476e-f579-40c4-a3f0-81f8104518b4
\.


--
-- Name: catalog_bundle_to_nested_product_offer bundle_to_nested_product_offer_uk01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_bundle_to_nested_product_offer
    ADD CONSTRAINT bundle_to_nested_product_offer_uk01 UNIQUE (bundle_product_offer, nested_product_offer);


--
-- Name: catalog_balance catalog_balance_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_balance
    ADD CONSTRAINT catalog_balance_pkey PRIMARY KEY (id);


--
-- Name: catalog_bundle_to_nested_product_offer catalog_bundle_to_nested_product_offer_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_bundle_to_nested_product_offer
    ADD CONSTRAINT catalog_bundle_to_nested_product_offer_pkey PRIMARY KEY (bundle_product_offer, nested_product_offer);


--
-- Name: catalog_calendar catalog_calendar_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_calendar
    ADD CONSTRAINT catalog_calendar_pkey PRIMARY KEY (id);


--
-- Name: catalog_calendar_properties catalog_calendar_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_calendar_properties
    ADD CONSTRAINT catalog_calendar_properties_pkey PRIMARY KEY (id);


--
-- Name: catalog_custom_list catalog_custom_list_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_custom_list
    ADD CONSTRAINT catalog_custom_list_pkey PRIMARY KEY (id);


--
-- Name: catalog_layout catalog_layout_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_layout
    ADD CONSTRAINT catalog_layout_pkey PRIMARY KEY (id);


--
-- Name: catalog_layout_to_layout_rating_parameters catalog_layout_to_layout_rating_parameters_uk01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_layout_to_layout_rating_parameters
    ADD CONSTRAINT catalog_layout_to_layout_rating_parameters_uk01 UNIQUE (catalog_layout, layout_rating_parameters);


--
-- Name: catalog_offer_price catalog_offer_price_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_offer_price
    ADD CONSTRAINT catalog_offer_price_pkey PRIMARY KEY (id);


--
-- Name: catalog_offer_term catalog_offer_term_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_offer_term
    ADD CONSTRAINT catalog_offer_term_pkey PRIMARY KEY (id);


--
-- Name: catalog_price_item catalog_price_item_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_price_item
    ADD CONSTRAINT catalog_price_item_pkey PRIMARY KEY (id);


--
-- Name: catalog_product_offering catalog_product_offering_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_pkey PRIMARY KEY (id);


--
-- Name: catalog_product catalog_product_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product
    ADD CONSTRAINT catalog_product_pkey PRIMARY KEY (id);


--
-- Name: catalog_provider catalog_provider_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_provider
    ADD CONSTRAINT catalog_provider_pkey PRIMARY KEY (id);


--
-- Name: catalog_rating_parameter catalog_rating_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter
    ADD CONSTRAINT catalog_rating_parameter_pkey PRIMARY KEY (id);


--
-- Name: catalog_rating_parameter_set catalog_rating_parameter_set_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter_set
    ADD CONSTRAINT catalog_rating_parameter_set_pkey PRIMARY KEY (id);


--
-- Name: catalog_rating_parameter_set_to_rating_parameter catalog_rating_parameter_set_to_rating_parameter_uk01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter_set_to_rating_parameter
    ADD CONSTRAINT catalog_rating_parameter_set_to_rating_parameter_uk01 UNIQUE (rating_parameter_set, rating_parameter);


--
-- Name: catalog_recurring_type catalog_recurring_type_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_recurring_type
    ADD CONSTRAINT catalog_recurring_type_pkey PRIMARY KEY (id);


--
-- Name: catalog_service catalog_service_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_service
    ADD CONSTRAINT catalog_service_pkey PRIMARY KEY (id);


--
-- Name: catalog_tariff catalog_tariff_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff
    ADD CONSTRAINT catalog_tariff_pkey PRIMARY KEY (id);


--
-- Name: catalog_tariff_plan catalog_tariff_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan
    ADD CONSTRAINT catalog_tariff_plan_pkey PRIMARY KEY (id);


--
-- Name: catalog_tariff_plan_to_calendar_range catalog_tariff_plan_to_tariff_calendar_range_uk01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan_to_calendar_range
    ADD CONSTRAINT catalog_tariff_plan_to_tariff_calendar_range_uk01 UNIQUE (catalog_tariff_plan, tariff_calendar_range);


--
-- Name: catalog_tariff_step catalog_tariff_step_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_step
    ADD CONSTRAINT catalog_tariff_step_pkey PRIMARY KEY (id);


--
-- Name: catalog_version catalog_version_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_version
    ADD CONSTRAINT catalog_version_pkey PRIMARY KEY (id);


--
-- Name: catalog_version_properties catalog_version_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_version_properties
    ADD CONSTRAINT catalog_version_properties_pkey PRIMARY KEY (version, catalog_object, reference_object);


--
-- Name: compatibility_set compatibility_set_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.compatibility_set
    ADD CONSTRAINT compatibility_set_pkey PRIMARY KEY (id);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: customer_account customer_account_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer_account
    ADD CONSTRAINT customer_account_pkey PRIMARY KEY (id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: eligibility_set eligibility_set_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.eligibility_set
    ADD CONSTRAINT eligibility_set_pkey PRIMARY KEY (id);


--
-- Name: eligibility_set_schema eligibility_set_schema_pk; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.eligibility_set_schema
    ADD CONSTRAINT eligibility_set_schema_pk PRIMARY KEY (id);


--
-- Name: exchange_rate exchange_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.exchange_rate
    ADD CONSTRAINT exchange_rate_pkey PRIMARY KEY (id);


--
-- Name: exchange_rate exchange_rate_uk01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.exchange_rate
    ADD CONSTRAINT exchange_rate_uk01 UNIQUE (realm, provider, currency);


--
-- Name: layout_rating_parameters_group layout_rating_parameters_group_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.layout_rating_parameters_group
    ADD CONSTRAINT layout_rating_parameters_group_pkey PRIMARY KEY (id);


--
-- Name: layout_rating_parameters layout_rating_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.layout_rating_parameters
    ADD CONSTRAINT layout_rating_parameters_pkey PRIMARY KEY (id);


--
-- Name: lcp_status lcp_status_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.lcp_status
    ADD CONSTRAINT lcp_status_pkey PRIMARY KEY (status);


--
-- Name: lcp_status_service lcp_status_service_pk; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.lcp_status_service
    ADD CONSTRAINT lcp_status_service_pk PRIMARY KEY (status, service);


--
-- Name: lookup lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.lookup
    ADD CONSTRAINT lookup_pkey PRIMARY KEY (id);


--
-- Name: lookup lookup_unq01; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.lookup
    ADD CONSTRAINT lookup_unq01 UNIQUE (module, key, effective_date_from);


--
-- Name: catalog_provider_service provider_service_pk; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_provider_service
    ADD CONSTRAINT provider_service_pk PRIMARY KEY (provider, service);


--
-- Name: rating_offer_details rating_offer_details_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.rating_offer_details
    ADD CONSTRAINT rating_offer_details_pkey PRIMARY KEY (id);


--
-- Name: recurring_offer_details recurring_offer_details_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.recurring_offer_details
    ADD CONSTRAINT recurring_offer_details_pkey PRIMARY KEY (id);


--
-- Name: subscriber_balance subscriber_balance_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_balance
    ADD CONSTRAINT subscriber_balance_pkey PRIMARY KEY (id);


--
-- Name: subscriber_offer subscriber_offer_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_offer
    ADD CONSTRAINT subscriber_offer_pkey PRIMARY KEY (id);


--
-- Name: subscriber subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT subscriber_pkey PRIMARY KEY (id);


--
-- Name: tariff_calendar_range tariff_calendar_range_pkey; Type: CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.tariff_calendar_range
    ADD CONSTRAINT tariff_calendar_range_pkey PRIMARY KEY (id);


--
-- Name: compatibility_set_to_product_offering_compatibility_set_id_prod; Type: INDEX; Schema: public; Owner: mcadmin
--

CREATE UNIQUE INDEX compatibility_set_to_product_offering_compatibility_set_id_prod ON public.compatibility_set_to_product_offering USING btree (compatibility_set_id, product_offering_id);


--
-- Name: lookup_key_idx; Type: INDEX; Schema: public; Owner: mcadmin
--

CREATE INDEX lookup_key_idx ON public.lookup USING btree (key);


--
-- Name: subscriber_id_idx; Type: INDEX; Schema: public; Owner: mcadmin
--

CREATE INDEX subscriber_id_idx ON public.rating_offer_details USING btree (subscriber_id);


--
-- Name: catalog_balance catalog_balance_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_balance
    ADD CONSTRAINT catalog_balance_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: catalog_bundle_to_nested_product_offer catalog_bundle_to_nested_product_offer_bundle_product_offer_fke; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_bundle_to_nested_product_offer
    ADD CONSTRAINT catalog_bundle_to_nested_product_offer_bundle_product_offer_fke FOREIGN KEY (bundle_product_offer) REFERENCES public.catalog_product_offering(id);


--
-- Name: catalog_calendar_properties catalog_calendar_properties_calendar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_calendar_properties
    ADD CONSTRAINT catalog_calendar_properties_calendar_fkey FOREIGN KEY (calendar) REFERENCES public.catalog_calendar(id);


--
-- Name: catalog_layout_to_layout_rating_parameters catalog_layout_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_layout_to_layout_rating_parameters
    ADD CONSTRAINT catalog_layout_fkey FOREIGN KEY (catalog_layout) REFERENCES public.catalog_layout(id);


--
-- Name: catalog_layout catalog_layout_rating_parameter_set_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_layout
    ADD CONSTRAINT catalog_layout_rating_parameter_set_fkey FOREIGN KEY (rating_parameter_set) REFERENCES public.catalog_rating_parameter_set(id);


--
-- Name: catalog_bundle_to_nested_product_offer catalog_nested_to_nested_product_offer_nested_product_offer_fke; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_bundle_to_nested_product_offer
    ADD CONSTRAINT catalog_nested_to_nested_product_offer_nested_product_offer_fke FOREIGN KEY (nested_product_offer) REFERENCES public.catalog_product_offering(id);


--
-- Name: catalog_offer_price catalog_offer_price_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_offer_price
    ADD CONSTRAINT catalog_offer_price_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: catalog_offer_term catalog_offer_term_recurring_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_offer_term
    ADD CONSTRAINT catalog_offer_term_recurring_type_fkey FOREIGN KEY (recurring_type) REFERENCES public.catalog_recurring_type(id);


--
-- Name: catalog_offer_term catalog_offer_term_tariff_plan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_offer_term
    ADD CONSTRAINT catalog_offer_term_tariff_plan_fkey FOREIGN KEY (tariff_plan) REFERENCES public.catalog_tariff_plan(id);


--
-- Name: catalog_price_item catalog_price_item_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_price_item
    ADD CONSTRAINT catalog_price_item_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: catalog_product_offering catalog_product_offering_offer_term_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_offer_term_fkey FOREIGN KEY (offer_term) REFERENCES public.catalog_offer_term(id);


--
-- Name: catalog_product_offering catalog_product_offering_periodic_balance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_periodic_balance_id_fkey FOREIGN KEY (periodic_balance_id) REFERENCES public.catalog_balance(id);


--
-- Name: catalog_product_offering catalog_product_offering_price_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_price_fkey FOREIGN KEY (price) REFERENCES public.catalog_offer_price(id);


--
-- Name: catalog_product_offering catalog_product_offering_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_product_fkey FOREIGN KEY (product) REFERENCES public.catalog_product(id);


--
-- Name: product_offering_to_compatibility_set catalog_product_offering_product_offering_to_compatibility_set_; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.product_offering_to_compatibility_set
    ADD CONSTRAINT catalog_product_offering_product_offering_to_compatibility_set_ FOREIGN KEY (product_offering_id) REFERENCES public.catalog_product_offering(id);


--
-- Name: catalog_product_offering catalog_product_offering_usage_balance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product_offering
    ADD CONSTRAINT catalog_product_offering_usage_balance_id_fkey FOREIGN KEY (usage_balance_id) REFERENCES public.catalog_balance(id);


--
-- Name: catalog_product catalog_product_service_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_product
    ADD CONSTRAINT catalog_product_service_fkey FOREIGN KEY (service) REFERENCES public.catalog_service(id);


--
-- Name: catalog_rating_parameter catalog_rating_parameter_custom_list_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter
    ADD CONSTRAINT catalog_rating_parameter_custom_list_fkey FOREIGN KEY (custom_list) REFERENCES public.catalog_custom_list(id);


--
-- Name: tariff_calendar_range catalog_tariff_calendar_range_catalog_calendar_properties_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.tariff_calendar_range
    ADD CONSTRAINT catalog_tariff_calendar_range_catalog_calendar_properties_fkey FOREIGN KEY (calendar_properties_id) REFERENCES public.catalog_calendar_properties(id);


--
-- Name: tariff_calendar_range catalog_tariff_calendar_range_catalog_tariff_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.tariff_calendar_range
    ADD CONSTRAINT catalog_tariff_calendar_range_catalog_tariff_fkey FOREIGN KEY (tariff_id) REFERENCES public.catalog_tariff(id);


--
-- Name: catalog_tariff_plan catalog_tariff_plan_calendar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan
    ADD CONSTRAINT catalog_tariff_plan_calendar_fkey FOREIGN KEY (calendar) REFERENCES public.catalog_calendar(id);


--
-- Name: catalog_tariff_plan_to_calendar_range catalog_tariff_plan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan_to_calendar_range
    ADD CONSTRAINT catalog_tariff_plan_fkey FOREIGN KEY (catalog_tariff_plan) REFERENCES public.catalog_tariff_plan(id);


--
-- Name: catalog_tariff_plan catalog_tariff_plan_layout_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan
    ADD CONSTRAINT catalog_tariff_plan_layout_fkey FOREIGN KEY (layout) REFERENCES public.catalog_layout(id);


--
-- Name: catalog_tariff_step catalog_tariff_step_price_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_step
    ADD CONSTRAINT catalog_tariff_step_price_item_fkey FOREIGN KEY (price_item) REFERENCES public.catalog_price_item(id);


--
-- Name: catalog_tariff_step catalog_tariff_step_tariff_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_step
    ADD CONSTRAINT catalog_tariff_step_tariff_fkey FOREIGN KEY (tariff) REFERENCES public.catalog_tariff(id);


--
-- Name: catalog_version_properties catalog_version_properties_version_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_version_properties
    ADD CONSTRAINT catalog_version_properties_version_fkey FOREIGN KEY (version) REFERENCES public.catalog_version(id);


--
-- Name: compatibility_set_to_product_offering compatibility_set_to_product_offering_catalog_product_offering_; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.compatibility_set_to_product_offering
    ADD CONSTRAINT compatibility_set_to_product_offering_catalog_product_offering_ FOREIGN KEY (product_offering_id) REFERENCES public.catalog_product_offering(id);


--
-- Name: compatibility_set_to_product_offering compatibility_set_to_product_offering_compatibility_set_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.compatibility_set_to_product_offering
    ADD CONSTRAINT compatibility_set_to_product_offering_compatibility_set_id_fk FOREIGN KEY (compatibility_set_id) REFERENCES public.compatibility_set(id);


--
-- Name: customer_account customer_account_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer_account
    ADD CONSTRAINT customer_account_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: customer_account customer_account_customer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer_account
    ADD CONSTRAINT customer_account_customer_fkey FOREIGN KEY (customer) REFERENCES public.customer(id);


--
-- Name: customer_account customer_account_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer_account
    ADD CONSTRAINT customer_account_parent_fkey FOREIGN KEY (parent) REFERENCES public.customer_account(id);


--
-- Name: customer customer_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_parent_fkey FOREIGN KEY (parent) REFERENCES public.customer(id);


--
-- Name: exchange_rate exchange_rate_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.exchange_rate
    ADD CONSTRAINT exchange_rate_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: exchange_rate exchange_rate_provider_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.exchange_rate
    ADD CONSTRAINT exchange_rate_provider_fkey FOREIGN KEY (provider) REFERENCES public.catalog_provider(id);


--
-- Name: catalog_layout_to_layout_rating_parameters layout_rating_parameters_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_layout_to_layout_rating_parameters
    ADD CONSTRAINT layout_rating_parameters_fkey FOREIGN KEY (layout_rating_parameters) REFERENCES public.layout_rating_parameters(id);


--
-- Name: layout_rating_parameters layout_rating_parameters_layout_rating_parameters_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.layout_rating_parameters
    ADD CONSTRAINT layout_rating_parameters_layout_rating_parameters_group_fkey FOREIGN KEY (group_id) REFERENCES public.layout_rating_parameters_group(id);


--
-- Name: layout_rating_parameters_to_parameter layout_rating_parameters_to_parameter_layout_rating_parameters_; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.layout_rating_parameters_to_parameter
    ADD CONSTRAINT layout_rating_parameters_to_parameter_layout_rating_parameters_ FOREIGN KEY (layout_rating_parameters) REFERENCES public.layout_rating_parameters(id);


--
-- Name: layout_tags layout_tags_layout_rating_parameters_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.layout_tags
    ADD CONSTRAINT layout_tags_layout_rating_parameters_fkey FOREIGN KEY (layout_rating_parameters) REFERENCES public.layout_rating_parameters(id);


--
-- Name: lcp_status_service lcp_status_service_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.lcp_status_service
    ADD CONSTRAINT lcp_status_service_status_fkey FOREIGN KEY (status) REFERENCES public.lcp_status(status);


--
-- Name: product_offering_to_compatibility_set product_offering_to_compatibility_set_compatibility_set_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.product_offering_to_compatibility_set
    ADD CONSTRAINT product_offering_to_compatibility_set_compatibility_set_id_fk FOREIGN KEY (compatibility_set_id) REFERENCES public.compatibility_set(id);


--
-- Name: product_offering_to_eligibility_set product_offering_to_eligibility_set_catalog_product_offering_id; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.product_offering_to_eligibility_set
    ADD CONSTRAINT product_offering_to_eligibility_set_catalog_product_offering_id FOREIGN KEY (product_offering_id) REFERENCES public.catalog_product_offering(id);


--
-- Name: product_offering_to_eligibility_set product_offering_to_eligibility_set_eligibility_set_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.product_offering_to_eligibility_set
    ADD CONSTRAINT product_offering_to_eligibility_set_eligibility_set_id_fk FOREIGN KEY (eligibility_set_id) REFERENCES public.eligibility_set(id);


--
-- Name: catalog_rating_parameter_set_to_rating_parameter rating_parameter_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter_set_to_rating_parameter
    ADD CONSTRAINT rating_parameter_fkey FOREIGN KEY (rating_parameter) REFERENCES public.catalog_rating_parameter(id);


--
-- Name: catalog_rating_parameter_set_to_rating_parameter rating_parameter_set_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_rating_parameter_set_to_rating_parameter
    ADD CONSTRAINT rating_parameter_set_fkey FOREIGN KEY (rating_parameter_set) REFERENCES public.catalog_rating_parameter_set(id);


--
-- Name: subscriber_balance subscriber_balance_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_balance
    ADD CONSTRAINT subscriber_balance_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency(id);


--
-- Name: subscriber_balance subscriber_balance_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_balance
    ADD CONSTRAINT subscriber_balance_parent_fkey FOREIGN KEY (parent) REFERENCES public.subscriber_balance(id);


--
-- Name: subscriber_balance subscriber_balance_subscriber_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_balance
    ADD CONSTRAINT subscriber_balance_subscriber_fkey FOREIGN KEY (subscriber) REFERENCES public.subscriber(id);


--
-- Name: subscriber subscriber_customer_account_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT subscriber_customer_account_fkey FOREIGN KEY (customer_account) REFERENCES public.customer_account(id);


--
-- Name: rating_offer_details subscriber_id_rating_offer_details_fk; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.rating_offer_details
    ADD CONSTRAINT subscriber_id_rating_offer_details_fk FOREIGN KEY (subscriber_id) REFERENCES public.subscriber(id);


--
-- Name: recurring_offer_details subscriber_id_recurring_offer_details_fk; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.recurring_offer_details
    ADD CONSTRAINT subscriber_id_recurring_offer_details_fk FOREIGN KEY (subscriber_id) REFERENCES public.subscriber(id);


--
-- Name: subscriber_offer subscriber_offer_product_offering_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_offer
    ADD CONSTRAINT subscriber_offer_product_offering_fkey FOREIGN KEY (product_offering) REFERENCES public.catalog_product_offering(id);


--
-- Name: subscriber_offer subscriber_offer_subscriber_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.subscriber_offer
    ADD CONSTRAINT subscriber_offer_subscriber_fkey FOREIGN KEY (subscriber) REFERENCES public.subscriber(id);


--
-- Name: catalog_tariff_plan_to_calendar_range tariff_calendar_range_fkey; Type: FK CONSTRAINT; Schema: public; Owner: mcadmin
--

ALTER TABLE ONLY public.catalog_tariff_plan_to_calendar_range
    ADD CONSTRAINT tariff_calendar_range_fkey FOREIGN KEY (tariff_calendar_range) REFERENCES public.tariff_calendar_range(id);


--
-- PostgreSQL database dump complete
--

